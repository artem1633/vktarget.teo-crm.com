<?php

namespace app\components;

use Yii;
use app\models\Advert;
use app\models\AdvertisingCompany;
use app\models\Settings;
use yii\helpers\VarDumper;

/**
 * Class VkAPI
 * @package app\components
 */
class VkAPI
{
    /**
     * @return mixed
     */
    public static function getAccounts()
    {
        $token = Settings::findByKey('vk_token')->value;
        $response = file_get_contents("https://api.vk.com/method/ads.getAccounts?access_token={$token}&v=5.102");

        return json_decode($response, true);
    }

    /**
     * @param integer $accountId
     * @return int
     */
    public static function getBalance($accountId)
    {
        $token = Settings::findByKey('vk_token')->value;
        $response = file_get_contents("https://api.vk.com/method/ads.getBudget?access_token={$token}&account_id={$accountId}&v=5.102");

        $response = json_decode($response, true);

        if(isset($response['response'])){
            return $response['response'];
        } else {
            return 0;
        }
    }

    public static function getCountries($needAll = 1, $count = 234)
    {
        $token = Settings::findByKey('vk_token')->value;

        $query = http_build_query([
            'access_token' => $token,
            'need_all' => $needAll,
            'count' => $count,
            'v' => '5.102',
        ]);

        $response = file_get_contents("https://api.vk.com/method/database.getCountries?".$query);

        return json_decode($response, true);
    }

    public static function getCities($countryId, $search)
    {
        $token = Settings::findByKey('vk_token')->value;

        $query = http_build_query([
            'access_token' => $token,
            'country_id' => $countryId,
            'q' => $search,
            'v' => '5.102',
        ]);

        $response = file_get_contents("https://api.vk.com/method/database.getCities?".$query);

        return json_decode($response, true);
    }

    public static function getCitiesById($ids)
    {
        $token = Settings::findByKey('vk_token')->value;

        $query = http_build_query([
            'access_token' => $token,
            'city_ids' => implode(',', $ids),
            'v' => '5.102',
        ]);

        $response = file_get_contents("https://api.vk.com/method/database.getCitiesById?".$query);

        return json_decode($response, true);
    }

    public static function getGroups($search, $count = 10)
    {
        $token = Settings::findByKey('vk_token')->value;

        $query = http_build_query([
            'access_token' => $token,
            'q' => $search,
            'count' => $count,
            'v' => '5.102',
        ]);

        $response = file_get_contents("https://api.vk.com/method/groups.search?".$query);

        return json_decode($response, true);
    }

    public static function getTargetGroups($accountId)
    {
        $token = Settings::findByKey('vk_token')->value;

        $query = http_build_query([
            'access_token' => $token,
            'account_id' => $accountId,
            'v' => '5.102',
        ]);

        $response = file_get_contents("https://api.vk.com/method/ads.getTargetGroups?".$query);

        return json_decode($response, true);
    }

    public static function getGroupsById($ids)
    {
        $token = Settings::findByKey('vk_token')->value;

        $query = http_build_query([
            'access_token' => $token,
            'groups_id' => $ids,
            'v' => '5.102',
        ]);

        $response = file_get_contents("https://api.vk.com/method/groups.getById?".$query);

        return json_decode($response, true);
    }

    public static function getCategories()
    {
        $token = Settings::findByKey('vk_token')->value;

        $query = http_build_query([
            'access_token' => $token,
            'v' => '5.102',
        ]);

        $response = file_get_contents("https://api.vk.com/method/ads.getCategories?".$query);

        return json_decode($response, true);
    }

//    public static function postAdsStealth($ownerId, $message)
//    {
//        $token = Settings::findByKey('vk_token')->value;
//        $response = file_get_contents("https://api.vk.com/method/wall.postAdsStealth?access_token={$token}&account_id={$accountId}&v=5.102");
//    }

    /**
     * @param integer $accountId
     * @param integer $campaignId
     * @param Advert $advert
     * @return array
     */
    public static function createAds($accountId, $campaignId, $advert)
    {
        $token = Settings::findByKey('vk_token')->value;

        if($advert->format != 9 && $advert->format != 8)
        {
            $response = self::getUploadUrl($advert->format);

            $uploadUrl = $response['response'];

            $photoLoaded = self::uploadFile($advert->photo, $uploadUrl);

            if($advert->photo_icon != null){
                $response = self::getUploadUrl($advert->format, true);

                $uploadUrl = $response['response'];

                $iconLoaded = self::uploadFile($advert->photo_icon, $uploadUrl);
            }
        } else {
            $photoLoaded = null;
        }

        // VarDumper::dump(json_decode($photoLoaded, true), 10, true)."<br>";

        // exit;

        // echo $campaignId;
        // exit;

        $options = [
            'campaign_id' => $campaignId,
            'ad_format' => $advert->format,
            'cost_type' => $advert->price_type,
            'cpc' => $advert->cpc,
            'cpm' => $advert->cpm,
            'ocpm' => $advert->ocpm,
            'goal_type' => $advert->goal_type,
            'link_url' => $advert->link_url,
            'photo' => json_decode($photoLoaded, true)['photo'],
            'age_from' => $advert->age_from,
            'age_to' => $advert->age_to,
            // 'link_button' => 'open',
            'category1' => 1,
        ];

        if(in_array($advert->format, [Advert::FORMAT_IMAGE_TEXT])){
            $options['title'] = $advert->title;
            $options['description'] = $advert->description;
        }

        if(in_array($advert->format, [Advert::FORMAT_BIG_IMAGE])){
            $options['title'] = $advert->title;
        }

        if($advert->format == Advert::FORMAT_COMMUNITY){
            $group = explode('/', $advert->link_url);

            $group = $group[count($group)-1];

            $screenName = self::resolveScreenName($group);

            $groupId = $screenName['response']['object_id'];

            $groupResponse = self::getGroupById($groupId, ['name']);

            $options['title'] = $groupResponse['response'][0]['name'];
        }


        if($advert->country != null){
            $options['country'] = $advert->country;
        }

        if($advert->cities != null){
            $options['cities'] = implode(',', $advert->cities);
        }

        if($advert->cities_not != null){
            $options['cities_not'] = implode(',', $advert->cities_not);
        }

        if($advert->groups != null){
            $options['groups'] = implode(',', $advert->groups);
        }

        if($advert->groups_not != null){
            $options['groups_not'] = implode(',', $advert->groups_not);
        }

        if($advert->groups_active != null){
            $options['groups_active'] = implode(',', $advert->groups_active);
        }

        if($advert->user_browsers != null){
            $options['user_browsers'] = implode(',', $advert->user_browsers);
        }

        if($advert->user_os != null){
            $options['user_os'] = implode(',', $advert->user_os);
        }

        if($advert->user_devices != null){
            $options['user_devices'] = implode(',', $advert->user_devices);
        }

        if($advert->positions != null){
            $options['positions'] = implode(',', $advert->positions);
        }

        if($advert->impressions_limit != null){
            $options['impressions_limit'] = $advert->impressions_limit;
        }

        if($advert->targetGroups != null){
            $options['retargeting_groups'] = implode(',', $advert->targetGroups);
        }

        if($advert->targetGroupsNot != null){
            $options['retargeting_groups_not'] = implode(',', $advert->targetGroupsNot);
        }

        if($advert->eventsRetargetingGroupsGroup != null){
            // $options['events_retargeting_groups'] = [
            // $advert->eventsRetargetingGroupsGroup => array_values($advert->eventsRetargetingGroupsEvent),
            // ];
            // $options["events_retargeting_groups"] = [27639385 => [3,4,20]];

            $eventsNumber = [];
            $events = $advert->eventsRetargetingGroupsEvent;

            foreach ($events as $event) {
                $eventsNumber[] = intval($event);
            }

            $options['events_retargeting_groups'] = [
                $advert->eventsRetargetingGroupsGroup => $eventsNumber,
            ];
        }

        if($advert->schools != null){
            $options['schools'] = implode(',', $advert->schools);
        }

        if($advert->apps != null){
            $options['apps'] = implode(',', $advert->apps);
        }

        if($advert->appsNot != null){
            $options['apps_not'] = implode(',', $advert->appsNot);
        }

        if($advert->ad_platform != null){
            $options['ad_platform'] = $advert->ad_platform;
        }


        $attributes = ['category1_id', 'category2_id', 'birthday', 'sex', 'family_statuses' => 'status', 'age_restriction', 'travellers'];

        $attributesKeys = array_keys($attributes);

        $counter = 0;
        foreach ($attributes as $attribute) {
            $attr = is_string($attributesKeys[$counter]) ? $attributesKeys[$counter] : $attribute;
            $optionName = $attribute;

            $options[$optionName] = $advert->$attr;

            $counter++;
        }

        // VarDumper::dump($options, 10, true);
        // exit;

        $data = [
            // [
            //     'campaign_id' => $campaignId,
            //     'ad_format' => $advert->format,
            //     'cost_type' => $advert->price_type,
            //     'cpc' => $advert->cpc,
            //     'cpm' => $advert->cpm,
            //     'ocpm' => $advert->ocpm,
            //     'goal_type' => $advert->goal_type,
            //     'link_url' => $advert->link_url,
            //     'photo' => json_decode($photoLoaded, true)['photo'],
            //     // 'photo_icon' => json_decode($iconLoaded, true)['photo'],
            //     // 'title' => $advert->title,
            //     // 'link_title' => 'Тестовая ссылка',
            //     // 'link_button' => 'open_url',
            //     // 'description' => $advert->description,
            // ]
            $options
        ];

        $query = http_build_query([
            'access_token' => $token,
            'account_id' => $accountId,
            'data' => json_encode($data),
            'v' => '5.102'
        ]);

        $response = file_get_contents("https://api.vk.com/method/ads.createAds?".$query);

        Yii::warning(json_decode($response, true));

        // VarDumper::dump($options, 10, true);

        // VarDumper::dump(json_decode($response, true), 10, true);

        // exit;
    }

    public static function getApps($q)
    {
        $token = Settings::findByKey('vk_token')->value;

        $query = http_build_query([
            'access_token' => $token,
            'sort' => 'visitors',
            'count' => '50',
            'q' => $q,
            'v' => '5.102'
        ]);

        $response = file_get_contents('https://api.vk.com/method/apps.getCatalog?'.$query);

        return json_decode($response, true);
    }

    /**
     * @param integer $accountId
     * @param Advert $advert
     * @param array $changedAttributes
     * @return array
     */
    public static function updateAds($accountId, $advert, $changedAttributes)
    {
        $token = Settings::findByKey('vk_token')->value;

        if($advert->format != 9 && $advert->format != 8)
        {
            $response = self::getUploadUrl($advert->format);

            $uploadUrl = $response['response'];

            $photoLoaded = self::uploadFile($advert->photo, $uploadUrl);

            if($advert->photo_icon != null){
                $response = self::getUploadUrl($advert->format, true);

                $uploadUrl = $response['response'];

                $iconLoaded = self::uploadFile($advert->photo_icon, $uploadUrl);
            }
        } else {
            $photoLoaded = null;
        }

        // VarDumper::dump(json_decode($photoLoaded, true), 10, true)."<br>";

        // exit;

        // echo $campaignId;
        // exit;

        $options = [
            'ad_id' => $advert->advert_id,
            'name' => $advert->name,
            'cost_type' => $advert->price_type,
            'cpc' => $advert->cpc,
            'cpm' => $advert->cpm,
            'ocpm' => $advert->ocpm,
            'goal_type' => $advert->goal_type,
            'link_url' => $advert->link_url,
            'photo' => json_decode($photoLoaded, true)['photo'],
            'age_from' => $advert->age_from,
            'age_to' => $advert->age_to,
            'day_limit' => $advert->day_limit,
            'all_limit' => $advert->general_limit,
            // 'link_button' => 'open',
            'category1' => 1,
        ];

        if(in_array('status', $changedAttributes)){
            $options['status'] = $advert->status;
        }

        if(in_array($advert->format, [Advert::FORMAT_IMAGE_TEXT])){
            $options['title'] = $advert->title;
            $options['description'] = $advert->description;
        }

        if($advert->format == Advert::FORMAT_COMMUNITY){
            $group = explode('/', $advert->link_url);

            $group = $group[count($group)-1];

            $screenName = self::resolveScreenName($group);

            $groupId = $screenName['response']['object_id'];

            $groupResponse = self::getGroupById($groupId, ['name']);

            $options['title'] = $groupResponse['response'][0]['name'];
        }


        $data = [
            // [
            //     'campaign_id' => $campaignId,
            //     'ad_format' => $advert->format,
            //     'cost_type' => $advert->price_type,
            //     'cpc' => $advert->cpc,
            //     'cpm' => $advert->cpm,
            //     'ocpm' => $advert->ocpm,
            //     'goal_type' => $advert->goal_type,
            //     'link_url' => $advert->link_url,
            //     'photo' => json_decode($photoLoaded, true)['photo'],
            //     // 'photo_icon' => json_decode($iconLoaded, true)['photo'],
            //     // 'title' => $advert->title,
            //     // 'link_title' => 'Тестовая ссылка',
            //     // 'link_button' => 'open_url',
            //     // 'description' => $advert->description,
            // ]
            $options
        ];

        $query = http_build_query([
            'access_token' => $token,
            'account_id' => $accountId,
            'data' => json_encode($data),
            'v' => '5.102'
        ]);

        $response = file_get_contents("https://api.vk.com/method/ads.updateAds?".$query);

        // VarDumper::dump(json_decode($response, true), 10, true);

        Yii::warning($response);

        // exit;

        return json_decode($response, true);
    }

    public static function getGroupById($id, $fields = [])
    {
        $token = Settings::findByKey('vk_token')->value;
        $query = http_build_query([
            'access_token' => $token,
            'group_id' => $id,
            'fields' => implode(',', $fields),
            'v' => '5.102'
        ]);

        $response = file_get_contents('https://api.vk.com/method/groups.getById?'.$query);

        return json_decode($response, true);
    }

    public static function getSuggestions($section, $q, $cities = null)
    {
        $token = Settings::findByKey('vk_token')->value;
        $query = http_build_query([
            'access_token' => $token,
            'section' => $section,
            // 'country' => $countryId,
            'q' => $q,
            'cities' => $cities,
            // 'v' => '5.101'
            'v' => '5.102'
        ]);

        $response = file_get_contents('https://api.vk.com/method/ads.getSuggestions?'.$query);

        // var_dump($response[0]['items']);

        return json_decode($response, true);
    }

    /**
     * @param integer $accountId
     * @param app\models\Advert $advert
     * @return mixed
     */
    public static function getStatistics($accountId, $advert)
    {
        $token = Settings::findByKey('vk_token')->value;
        $query = http_build_query([
            'account_id' => $accountId,
            'access_token' => $token,
            'ids_type' => 'ad',
            'ids' => $advert->advert_id,
            'period' => 'overall',
            'date_from' => '0',
            'date_to' => '0',
            'ad_platform' => 'all',
            'v' => '5.102'
        ]);

        $response = file_get_contents('https://api.vk.com/method/ads.getStatistics?'.$query);

        return json_decode($response, true);
    }

    /**
     * @param integer $adFormat
     * @return mixed
     */
    public static function getUploadUrl($adFormat, $icon = false)
    {
        $token = Settings::findByKey('vk_token')->value;
        $query = http_build_query([
            'access_token' => $token,
            'ad_format' => $adFormat,
            'icon' => $icon ? 1 : 0,
            'v' => '5.102'
        ]);

        $response = file_get_contents('https://api.vk.com/method/ads.getUploadURL?'.$query);

        return json_decode($response, true);
    }

    /**
     * @param string $filePath
     * @param string $uploadUrl
     * @return mixed
     */
    public static function uploadFile($filePath, $uploadUrl)
    {
        // $ch = curl_init($filePath);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_HEADER, false);
        // $html = curl_exec($ch);
        // curl_close($ch);
        // file_put_contents(basename($filePath), $html);


        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL =>  $uploadUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                'Content-Type: multipart/form-data'
            ],
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => [
                'file' => curl_file_create($filePath, mime_content_type($filePath), $filePath)
            ]
        ]);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    /**
     * @param integer $owner_id
     * @param string $message
     * @param integer $signed
     * @return mixed
     */
    public static function postAdsStealth($owner_id, $message, $signed)
    {
        $token = Settings::findByKey('vk_token')->value;
        $query = http_build_query([
            'access_token' => $token,
            'owner_id' => $owner_id,
            'message' => $message,
            'signed' => $signed,
            'v' => '5.102'
        ]);

        $response = file_get_contents('https://api.vk.com/method/wall.postAdsStealth?'.$query);

        return json_decode($response, true);
    }

    /**
     * @param string $screenName
     * @return mixed
     */
    public static function resolveScreenName($screenName)
    {
        $token = Settings::findByKey('vk_token')->value;
        $query = http_build_query([
            'access_token' => $token,
            'screen_name' => $screenName,
            'v' => '5.102'
        ]);

        $response = file_get_contents('https://api.vk.com/method/utils.resolveScreenName?'.$query);

        return json_decode($response, true);
    }

    /**
     * @param integer $accountId
     * @param AdvertisingCompany $company
     * @return array
     */
    public static function createCampaign($accountId, $company)
    {
        $token = Settings::findByKey('vk_token')->value;
        $data = json_encode([$company->vkTransfer->transfer()]);

        $query = http_build_query([
            'access_token' => $token,
            'data' => $data,
            'account_id' => $accountId,
            'v' => '5.102'
        ]);

        $response = file_get_contents("https://api.vk.com/method/ads.createCampaigns?".$query);
//        $response = [];

        return json_decode($response, true);
    }
}