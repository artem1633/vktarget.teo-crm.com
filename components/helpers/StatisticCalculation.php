<?php

namespace app\components\helpers;

/**
 * Class StatisticCalculation
 * @package app\components\helpers
 */
class StatisticCalculation
{
    /**
     * @param integer $views
     * @param integer $clicks
     * @return double
     */
    public static function calculateCTR($views, $clicks)
    {
        return $clicks / $views;
    }

    /**
     * @param double $spent
     * @param integer $clicks
     * @return double
     */
    public static function calculateECPC($spent, $clicks)
    {
        return $spent / $clicks;
    }
}