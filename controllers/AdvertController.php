<?php

namespace app\controllers;

use Yii;
use app\models\Advert;
use app\models\AdvertSearch;
use app\models\AdvertisingCompany;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\forms\VkPostForm;
use app\actions\EditableColumnAction;
use yii\helpers\ArrayHelper;
use app\components\VkAPI;
use app\models\Settings;

/**
 * AdvertController implements the CRUD actions for Advert model.
 */
class AdvertController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'edit' => [                                       // identifier for your editable action
                'class' => EditableColumnAction::className(),     // action class name
                'modelClass' => Advert::className(),                // the update model class
                'outputValue' => function ($model, $attribute, $key, $index) {
                    if($attribute == 'status'){
                        return ArrayHelper::getValue(\app\models\Advert::statusLabels(), $model->status);
                    }

                    return $model->$attribute;
                },
                'scenario' => function($model){
                    return $model->getLastScenario();
                }
            ]
        ]);
    }

    /**
     * Lists all Advert models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdvertSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $dataProvider->query->andWhere(['user_id' => Yii::$app->user->getId()]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Advert model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "объявление #".$id,
                'content'=>$this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Advert model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($companyId = null)
    {
        $request = Yii::$app->request;
        $model = new Advert(['scenario' => Advert::SCENARIO_FORMAT, 'step' => -1]);
        $model->company_id = $companyId;
        $company = AdvertisingCompany::findOne($companyId);

        if($request->isPost){
            $model->scenario = $_POST['scenario'];
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить объявление",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'company' => $company,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить объявление",
                    'content'=>'<span class="text-success">Создание объявления успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Добавить объявление",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'company' => $company,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->nextStep()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'company' => $company,
                ]);
            }
        }
    }

    /**
     * @return mixed
     */
    public function actionVkCitiesSearch($country_id, $q)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = VkAPI::getCities($country_id, $q);

        if(isset($response['response'])){
            return $response['response'];
        }

        return $response;
    }

    /**
     * @return mixed
     */
    public function actionVkGroupsSearch($q)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = VkAPI::getGroups($q);


        return ['count' => $response['response']['count'], 'items' => $response['response']['items']];
    }

    public function actionVkAppsSearch($q)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = VkAPI::getApps($q);

        return ['count' => $response['response']['count'], 'items' => $response['response']['items']];
    }

    public function actionVkPositions($q)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = VkAPI::getSuggestions('positions', $q);


        return ['count' => count($response['response']), 'items' => $response['response']];
    }

    public function actionVkTargetGroups()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $accountId = Settings::findByKey('account_id')->value;
        $response = VkAPI::getTargetGroups($accountId);

        return ['count' => count($response['response']), 'items' => $response['response']];
    }

    public function actionVkSchoolsSearch($q, $citiesIds)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $accountId = Settings::findByKey('account_id')->value;
        $response = VkAPI::getSuggestions('schools', $q, $citiesIds);

        return ['count' => count($response['response']), 'items' => $response['response']];
    }

    public function actionCreatePost()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new VkPostForm();

        if($model->load($request->post()) && $model->post()){
            return [
                'forceClose' => true,
                'linkUrl' => $model->getPostLink(),
            ];
        } else {
            return [
                'title'=> "Создать пост",
                'content'=>$this->renderAjax('create_post_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit", 'data-response-callback' => 'function(response){ $("#advert-link_url").val(response.linkUrl); }'])
            ];
        }
    }

    /**
     * Updates an existing Advert model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $company = AdvertisingCompany::findOne($model->company_id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить объявление #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'company' => $company,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "объявление #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                        'company' => $company,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                return [
                    'title'=> "Изменить объявление #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'company' => $company,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'company' => $company,
                ]);
            }
        }
    }

    /**
     * Delete an existing Advert model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Advert model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Advert model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Advert the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Advert::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
