<?php

/**
 * @var $this \yii\web\View
 * @var $settings \app\models\Settings[]
 */

$this->title = 'Настройки';

$accountId = \app\models\Settings::findByKey('account_id')->value;
if($accountId == null){
    $accountBalance = 0;
} else {
    $accountBalance = \app\components\VkAPI::getBalance($accountId);
}

?>


<div class="panel panel-inverse news-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title">Новости</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <?php if (count($settings) > 0) { ?>
                    <form method="post" class="form-horizontal form-bordered" style="padding: 0;">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                               value="<?= Yii::$app->request->csrfToken ?>">
                        <?php foreach ($settings as $setting): ?>
                            <div class="form-group">
                                <label for=""
                                       class="control-label col-md-2"><?= Yii::$app->formatter->asRaw($setting->label) ?></label>
                                <div class="col-md-10">
                                    <div class="col-md-8">
                                            <input name="Settings[<?= $setting->key ?>]" type="text"
                                                   value="<?= $setting->value ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="form-group">
                            <label for="" class="control-label col-md-2">
                                Баланс кабинета: <?= Yii::$app->formatter->asCurrency($accountBalance, 'rub') ?>
                            </label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <button type="submit" style="margin-left: 15px;"
                                            class="btn btn-success btn-sm">Сохранить
                                    </button>
                                    <a style="margin-left: 15px;" href="https://oauth.vk.com/authorize?client_id=6651556&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=offline,groups,ads&response_type=token&v=5.37" class="btn btn-primary btn-sm" target="_blank">Получить ВК токен</a>
                                    <a id="account-id-button" href="<?=\yii\helpers\Url::toRoute(['settings/get-account-id'])?>" style="margin-left: 15px;" class="btn btn-info btn-sm">Получить ID рекламного кабинета</a>
                                    <input id="account-id-fld" type="text" disabled="disabled" class="form-control" style="display: none; width: 22%; height: 30px; float: right; cursor: pointer">
                                </div>
                            </div>
                        </div>
                    </form>
                <?php } else { ?>
                    <div class="note note-danger" style="margin: 15px;">
                        <h4><i class="fa fa-exclamation-triangle"></i> Настроек в базе данных не обнаружено!
                        </h4>
                        <p>
                            Убедитесь, что все миграции выполнены без ошибок
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php

$script = <<< JS
$('#account-id-button').click(function(e){
    e.preventDefault();
    $.ajax({
        'method': 'GET',
        'url': $(this).attr('href'),
        success: function(response){
            $('#account-id-fld').slideDown('fast');
            $('#account-id-fld').val(response);  
        },
        error: function(error, error2){
          $('#account-id-fld').slideDown('fast');
          $('#account-id-fld').val(error.responseJSON.message);  
        }
    });
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>