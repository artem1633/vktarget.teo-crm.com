<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserRole */
?>
<div class="user-role-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'max_month_credits',
            'max_day_credits',
            'max_rate_click',
            'max_thousand_rate_view',
            'max_credit_speed_in_value',
            'max_credit_speed_in_min',
            'created_at',
        ],
    ]) ?>

</div>
