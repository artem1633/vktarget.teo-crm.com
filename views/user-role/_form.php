<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserRole */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-role-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'max_month_credits')->textInput() ?>

    <?= $form->field($model, 'max_day_credits')->textInput() ?>

    <?= $form->field($model, 'max_rate_click')->textInput() ?>

    <?= $form->field($model, 'max_thousand_rate_view')->textInput() ?>

    <?= $form->field($model, 'max_credit_speed_in_value')->textInput() ?>

    <?= $form->field($model, 'max_credit_speed_in_min')->textInput() ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
