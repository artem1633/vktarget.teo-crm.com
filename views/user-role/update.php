<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserRole */
?>
<div class="user-role-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
