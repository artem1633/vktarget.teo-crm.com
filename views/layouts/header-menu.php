<?php

use yii\helpers\Url;

?>


<div id="sidebar" class="sidebar">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\Menu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Компании', 'icon' => 'fa  fa-th', 'url' => ['/advertising-company']],
//                    ['label' => 'Объявления', 'icon' => 'fa  fa-newspaper-o', 'url' => ['/advert']],
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Роли пользователей', 'icon' => 'fa  fa-users', 'url' => ['/user-role'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Настройки', 'icon' => 'fa  fa-cog', 'url' => ['/settings'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ['label' => 'Тикеты', 'icon' => 'fa fa-question', 'url' => ['/ticket'],],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
