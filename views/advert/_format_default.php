<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Advert;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use app\models\AdvertisingCompany;

/* @var $this yii\web\View */
/* @var $model app\models\Advert */
/* @var $company app\models\AdvertisingCompany */
/* @var $form yii\widgets\ActiveForm */

if(Yii::$app->user->identity->isSuperAdmin()){
    $companiesList = ArrayHelper::map(AdvertisingCompany::find()->all(), 'id', 'name');
} else {
    $companiesList = ArrayHelper::map(AdvertisingCompany::find()->where(['user_id' => Yii::$app->user->getId()])->all(), 'id', 'name');
}

CrudAsset::register($this);

?>

    <div class="advert-form">

        <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['enctype' => 'multipart/form-data']]); ?>


        <?php if($model->format == null): ?>
            <div class="step-wrapper" <?=$model->scenario == Advert::SCENARIO_FORMAT ? '' : 'style="display: none;"'?>>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'format')->dropDownList(Advert::formatLabels($company->type)) ?>
                    </div>
                </div>
            </div>


            <div class="hidden">
                <?= $form->field($model, 'step')->hiddenInput() ?>
                <input type="hidden" name="scenario" value="<?=$model->scenario?>">
            </div>


            <?php if (!Yii::$app->request->isAjax){ ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isCurrentLastStep() ? 'Создать' : 'Далее', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            <?php } ?>

        <?php else: ?>
            <?php if($model->format == Advert::FORMAT_IMAGE_TEXT): ?>
                <?= $this->render('_format_image_text', [
                    'model' => $model,
                    'company' => $company,
                    'form' => $form
                ]) ?>
            <?php endif; ?>
            <?php if($model->format == Advert::FORMAT_COMMUNITY): ?>
                <?= $this->render('_format_community', [
                    'model' => $model,
                    'company' => $company,
                    'form' => $form
                ]) ?>
            <?php endif; ?>
            <?php if($model->format == Advert::FORMAT_BIG_IMAGE): ?>
                <?= $this->render('_format_big_image', [
                    'model' => $model,
                    'company' => $company,
                    'form' => $form
                ]) ?>
            <?php endif; ?>
        <?php endif; ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


<?php

$script = <<< JS
$("#advert-price_type").change(function(){
    var val = parseInt($(this).val());
    console.log(val);
    if(val === 1) {
        $('.cpm-wrapper').show();
        $('.cpc-wrapper').hide();
    } else if(val === 0) {
        $('.cpc-wrapper').show();
        $('.cpm-wrapper').hide();
    }
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>