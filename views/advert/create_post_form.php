<?php
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\VkPostForm */
?>

<div class="vk_post-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'ownerLink')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'message')->textarea() ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'signed')->checkbox() ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
