<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Advert;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use app\models\AdvertisingCompany;

/* @var $this yii\web\View */
/* @var $model app\models\Advert */
/* @var $company app\models\AdvertisingCompany */
/* @var $form yii\widgets\ActiveForm */

if(Yii::$app->user->identity->isSuperAdmin()){
    $companiesList = ArrayHelper::map(AdvertisingCompany::find()->all(), 'id', 'name');
} else {
    $companiesList = ArrayHelper::map(AdvertisingCompany::find()->where(['user_id' => Yii::$app->user->getId()])->all(), 'id', 'name');
}

CrudAsset::register($this);

?>

    <div class="advert-form">

        <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'format')->dropDownList(Advert::formatLabels($company->type)) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'title')->textInput() ?>
            </div>
        </div>

        <?php if($company->type != AdvertisingCompany::TYPE_PROMOTED_POSTS): ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'description')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'title')->textInput() ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'goal_type')->dropDownList(Advert::goalLabels()) ?>
            </div>
            <?php if($company->type == AdvertisingCompany::TYPE_PROMOTED_POSTS): ?>
                <div class="col-md-7">
                    <?= $form->field($model, 'link_url')->textInput() ?>
                </div>
                <div class="col-md-1">
                    <?= Html::a('Создать запись', 'create-post', ['class' => 'btn btn-sm btn-success', 'style' => 'margin-top: 27px;', 'role' => 'modal-remote']) ?>
                </div>
            <?php else: ?>
                <div class="col-md-8">
                    <?= $form->field($model, 'link_url')->textInput() ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?php
                if($company->type == AdvertisingCompany::TYPE_PROMOTED_POSTS){
                    $model->price_type = Advert::PRICE_TYPE_VIEW;
                }
                ?>
                <?php if($company->type == AdvertisingCompany::TYPE_PROMOTED_POSTS): ?>
                    <?= $form->field($model, 'price_type')->dropDownList(Advert::priceTypeLabels($company->type), ['prompt' => 'Выберите тип цены', 'disabled' => 'disabled']) ?>
                    <?= $form->field($model, 'price_type')->hiddenInput()->label(false) ?>
                <?php else: ?>
                    <?= $form->field($model, 'price_type')->dropDownList(Advert::priceTypeLabels($company->type), ['prompt' => 'Выберите тип цены']) ?>
                <?php endif; ?>
            </div>
            <div class="cpc-wrapper" <?= $model->price_type == 1 ? 'style="display: none;"' : '' ?>>
                <div class="col-md-4">
                    <?= $form->field($model, 'cpc')->textInput() ?>
                </div>
            </div>
            <div class="cpm-wrapper" <?= $model->price_type == 0 ? 'style="display: none;"' : '' ?>>
                <div class="col-md-4">
                    <?= $form->field($model, 'cpm')->textInput() ?>
                </div>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'ocpm')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <?php if($company->type != AdvertisingCompany::TYPE_PROMOTED_POSTS): ?>
                <div class="col-md-3">
                    <?= $form->field($model, 'photo')->fileInput() ?>
                </div>
            <?php endif; ?>
            <div class="col-md-3">
                <?= $form->field($model, 'status')->dropDownList(Advert::statusLabels()) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'age_from')->input('number') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'age_to')->input('number') ?>
            </div>
        </div>



        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


<?php

$script = <<< JS
$("#advert-price_type").change(function(){
    var val = parseInt($(this).val());
    console.log(val);
    if(val === 1) {
        $('.cpm-wrapper').show();
        $('.cpc-wrapper').hide();
    } else if(val === 0) {
        $('.cpc-wrapper').show();
        $('.cpm-wrapper').hide();
    }
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>