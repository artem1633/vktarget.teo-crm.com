<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Advert;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use app\models\AdvertisingCompany;
use app\components\VkAPI;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\models\Settings;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Advert */
/* @var $company app\models\AdvertisingCompany */
/* @var $form yii\widgets\ActiveForm */

if(Yii::$app->user->identity->isSuperAdmin()){
    $companiesList = ArrayHelper::map(AdvertisingCompany::find()->all(), 'id', 'name');
} else {
    $companiesList = ArrayHelper::map(AdvertisingCompany::find()->where(['user_id' => Yii::$app->user->getId()])->all(), 'id', 'name');
}

CrudAsset::register($this);

?>

    <div class="advert-form">


        <div class="step-wrapper" <?=$model->scenario == Advert::SCENARIO_BIG_IMAGE_TITLE ? '' : 'style="display: none;"'?>>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'title')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'link_url')->textInput() ?>
                </div>
            </div>
            <div class="hidden">
                <?php $model->goal_type = Advert::GOAL_TYPE_TRANSFER; ?>
                <?= $form->field($model, 'goal_type')->dropDownList(Advert::goalLabels()) ?>
            </div>
        </div>

        <div class="step-wrapper" <?=$model->scenario == Advert::SCENARIO_BIG_IMAGE_PHOTO ? '' : 'style="display: none;"'?>>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'photo')->fileInput() ?>
                </div>
            </div>
        </div>

        <div class="step-wrapper" <?=$model->scenario == Advert::SCENARIO_BIG_IMAGE_PRICE_TRANSFER ? '' : 'style="display: none;"'?>>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'price_type')->dropDownList(Advert::priceTypeLabels($company->type), ['disabled' => 'disabled']) ?>
                    <?php if($company->type == AdvertisingCompany::TYPE_NORMAL): ?>'
                        <?php $model->price_type = Advert::PRICE_TYPE_TRANSFER; ?>
                        <?= $form->field($model, 'price_type')->hiddenInput()->label(false) ?>
                    <?php endif; ?>
                    <?= $form->field($model, 'price_type')->hiddenInput()->label(false) ?>
                </div>
                <div class="cpc-wrapper" <?= $model->price_type == 1 ? 'style="display: none;"' : '' ?>>
                    <div class="col-md-4">
                        <?= $form->field($model, 'cpc')->textInput() ?>
                    </div>
                </div>
                <div class="cpm-wrapper" <?= $model->price_type == 0 ? 'style="display: none;"' : '' ?>>
                    <div class="col-md-8">
                        <?= $form->field($model, 'cpm')->textInput() ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="step-wrapper" <?=$model->scenario == Advert::SCENARIO_BIG_IMAGE_TARGETING ? '' : 'style="display: none;"'?>>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $categoryData = [];
                    if(Advert::getScenarioIndex($model->scenario) >= Advert::getScenarioIndex(Advert::SCENARIO_BIG_IMAGE_TARGETING)){
                        $categories = VkAPI::getCategories();
                        if(isset($categories['response']['v1'])){
                            $cats = [];

                            foreach ($categories['response']['v1'] as $cat) {
                                $cats[$cat['id']] = $cat['name'];
                                if(isset($cat['subcategories']) == false){
                                    continue;
                                }
                                foreach ($cat['subcategories'] as $subcat) {
                                    $cats[$subcat['id']] = $subcat['name'];
                                }
                            }

                            $categoryData = $cats;
                        }
                    }
                    ?>
                    <?= $form->field($model, 'category1_id')->widget(Select2::className(), [
                        'data' => $categoryData,
                    ]) ?>
                    <?= $form->field($model, 'category2_id')->widget(Select2::className(), [
                        'data' => $categoryData,
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'age_from')->input('number') ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'age_to')->input('number') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $countryData = [];
                    if(Advert::getScenarioIndex($model->scenario) >= Advert::getScenarioIndex(Advert::SCENARIO_BIG_IMAGE_TARGETING)) {
                        $countries = VkAPI::getCountries();
                        if(isset($countries['response']['items'])){
                            $countryData = ArrayHelper::map($countries['response']['items'], 'id', 'title');
                        }
                    }
                    ?>
                    <?= $form->field($model, 'country')->widget(Select2::className(), [
                        'data' => $countryData,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'placeholder' => 'Выберите страну',
                        ],
                        'pluginEvents' => [
                            // 'select2:select' => 'function(){ alert($(this).val()); }',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $formatJs = <<< JS
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.title;
    }

	if(repo.region != undefined){
		var region = '<b style="margin-left: 5px;">' + repo.region;
		if(repo.area != undefined){
			region += ', ' + repo.area;
		}
		region += '</b>';
	} else {
		var region = '';
	}

    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<p style="margin-left: 5px; margin-bottom: 3px; font-size: 15px;">' + repo.title + '</p>' +
        region + 
    '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.title;
}
JS;

                    // Register the formatting script
                    $this->registerJs($formatJs, \yii\web\View::POS_HEAD);


                    // script to parse the results into the format expected by Select2
                    $resultsJs = "
                            function (data, params) {
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.count
                                    }
                                };
                            }
                            ";
                    ?>
                    <?= $form->field($model, 'cities')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Выберите города', 'multiple' => true],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => Url::toRoute(['advert/vk-cities-search']),
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) {
                                        var countryId = $("#advert-country").val();

                                        return {q:params.term, country_id: countryId};
                                    }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'minimumInputLength' => 3,
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatRepo'),
                            'templateSelection' => new JsExpression('formatRepoSelection'),
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $formatJs = <<< JS
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.title;
    }

    if(repo.region != undefined){
        var region = '<b style="margin-left: 5px;">' + repo.region;
        if(repo.area != undefined){
            region += ', ' + repo.area;
        }
        region += '</b>';
    } else {
        var region = '';
    }

    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<p style="margin-left: 5px; margin-bottom: 3px; font-size: 15px;">' + repo.title + '</p>' +
        region + 
    '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.title;
}
JS;

                    // Register the formatting script
                    $this->registerJs($formatJs, \yii\web\View::POS_HEAD);


                    // script to parse the results into the format expected by Select2
                    $resultsJs = "
                            function (data, params) {
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.count
                                    }
                                };
                            }
                            ";
                    ?>
                    <?= $form->field($model, 'cities_not')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Выберите города', 'multiple' => true],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => Url::toRoute(['advert/vk-cities-search']),
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) {
                                        var countryId = $("#advert-country").val();

                                        return {q:params.term, country_id: countryId};
                                    }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'minimumInputLength' => 3,
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatRepo'),
                            'templateSelection' => new JsExpression('formatRepoSelection'),
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $formatJs = <<< JS
var formatRepo = function (repo) {
    console.log(repo);
    if (repo.loading) {
        return repo.text;
    }

    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<img src="' + repo.photo_50 + '" style="display: inline-block; width: 35px; height: 35px; border-radius: 100%;">' +
        '<p style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 15px;">' + repo.name + '</p>' +
    '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.name;
}
JS;

                    // Register the formatting script
                    $this->registerJs($formatJs, \yii\web\View::POS_HEAD);


                    // script to parse the results into the format expected by Select2
                    $resultsJs = "
                            function (data, params) {
                                data.items = Object.values(data.items);
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.count
                                    }
                                };
                            }
                            ";
                    ?>
                    <?= $form->field($model, 'groups')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Выберите группы', 'multiple' => true],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => Url::toRoute(['advert/vk-groups-search']),
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) {
                                        var countryId = $("#advert-country").val();

                                        return {q:params.term};
                                    }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'minimumInputLength' => 3,
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatRepo'),
                            'templateSelection' => new JsExpression('formatRepoSelection'),
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $formatJs = <<< JS
var formatRepo = function (repo) {
    console.log(repo);
    if (repo.loading) {
        return repo.text;
    }

    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<img src="' + repo.photo_50 + '" style="display: inline-block; width: 35px; height: 35px; border-radius: 100%;">' +
        '<p style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 15px;">' + repo.name + '</p>' +
    '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.name;
}
JS;

                    // Register the formatting script
                    $this->registerJs($formatJs, \yii\web\View::POS_HEAD);


                    // script to parse the results into the format expected by Select2
                    $resultsJs = "
                            function (data, params) {
                                data.items = Object.values(data.items);
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.count
                                    }
                                };
                            }
                            ";
                    ?>
                    <?= $form->field($model, 'groups_not')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Выберите группы', 'multiple' => true],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => Url::toRoute(['advert/vk-groups-search']),
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) {
                                        var countryId = $("#advert-country").val();

                                        return {q:params.term};
                                    }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'minimumInputLength' => 3,
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatRepo'),
                            'templateSelection' => new JsExpression('formatRepoSelection'),
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $formatJs = <<< JS
var formatRepo = function (repo) {
    console.log(repo);
    if (repo.loading) {
        return repo.text;
    }

    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<img src="' + repo.photo_50 + '" style="display: inline-block; width: 35px; height: 35px; border-radius: 100%;">' +
        '<p style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 15px;">' + repo.name + '</p>' +
    '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.name;
}
JS;

                    // Register the formatting script
                    $this->registerJs($formatJs, \yii\web\View::POS_HEAD);


                    // script to parse the results into the format expected by Select2
                    $resultsJs = "
                            function (data, params) {
                                data.items = Object.values(data.items);
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.count
                                    }
                                };
                            }
                            ";
                    ?>
                    <?= $form->field($model, 'groups_active')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Выберите группы', 'multiple' => true],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => Url::toRoute(['advert/vk-groups-search']),
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) {
                                        var countryId = $("#advert-country").val();

                                        return {q:params.term};
                                    }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'minimumInputLength' => 3,
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatRepo'),
                            'templateSelection' => new JsExpression('formatRepoSelection'),
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $formatJs = <<< JS
var formatRepo = function (repo) {
    console.log(repo);
    if (repo.loading) {
        return repo.text;
    }

    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<p style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 15px;">' + repo.name + '</p><br>' +
        '<p style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 1px; font-size: 13px;">' + repo.desc + '</p><br>' +
        '<b style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 1px; font-size: 12px;">' + repo.parent + '</b>' +
    '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.name;
}
JS;

                    // Register the formatting script
                    $this->registerJs($formatJs, \yii\web\View::POS_HEAD);


                    // script to parse the results into the format expected by Select2
                    $resultsJs = "
                            function (data, params) {
                                data.items = Object.values(data.items);
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.count
                                    }
                                };
                            }
                            ";
                    ?>
                    <?= $form->field($model, 'schools')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Выберите школы', 'multiple' => true],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => Url::toRoute(['advert/vk-schools-search']),
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) {
                                        var cityIds = $("#advert-cities").val();
                                        var countryId = $("#advert-country").val();

                                        cityIds = cityIds.join(",");

                                        return {q:params.term, "citiesIds": cityIds, "countryId": countryId};
                                    }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'minimumInputLength' => 3,
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatRepo'),
                            'templateSelection' => new JsExpression('formatRepoSelection'),
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php

                    $accountId = Settings::findByKey('account_id')->value;
                    $response = VkAPI::getTargetGroups($accountId);

                    $targetGroups = ArrayHelper::map($response['response'], 'id', 'name');

                    ?>
                    <?= $form->field($model, 'targetGroups')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Выберите группы', 'multiple' => true],
                        'data' => $targetGroups,
                        'pluginOptions' => [
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'targetGroupsNot')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Выберите группы', 'multiple' => true],
                        'data' => $targetGroups,
                        'pluginOptions' => [
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'eventsRetargetingGroups')->widget(Select2::className(), [
                        'data' => $model->eventsRetargetingGroupsLabels(),
                        'options' => ['multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [','],
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'ad_platform')->dropDownList($model->adPlatformLabels()) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'user_browsers')->widget(Select2::class, [
                        'data' => $model->userBrowsersLabels(),
                        'options' => ['multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [','],
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'user_os')->widget(Select2::class, [
                        'data' => $model->userOsLabels(),
                        'options' => ['multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [','],
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'user_devices')->widget(Select2::class, [
                        'data' => $model->userDevicesLabels(),
                        'options' => ['multiple' => true],
                        'pluginOptions' => [
                            'tags' => true,
                            'tokenSeparators' => [','],
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $formatJs = <<< JS
var formatRepo = function (repo) {
    console.log(repo);
    if (repo.loading) {
        return repo.text;
    }

    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<img src="' + repo.icon_75 + '" style="display: inline-block; width: 35px; height: 35px; border-radius: 100%;">' +
        '<p style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 15px;">' + repo.title + '</p>' +
    '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.title;
}
JS;

                    // Register the formatting script
                    $this->registerJs($formatJs, \yii\web\View::POS_HEAD);


                    // script to parse the results into the format expected by Select2
                    $resultsJs = "
                            function (data, params) {
                                data.items = Object.values(data.items);
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.count
                                    }
                                };
                            }
                            ";
                    ?>
                    <?= $form->field($model, 'apps')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Выберите приложения', 'multiple' => true],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => Url::toRoute(['advert/vk-apps-search']),
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) {
                                        return {q:params.term};
                                    }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'minimumInputLength' => 3,
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatRepo'),
                            'templateSelection' => new JsExpression('formatRepoSelection'),
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $formatJs = <<< JS
var formatRepo = function (repo) {
    console.log(repo);
    if (repo.loading) {
        return repo.text;
    }

    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<img src="' + repo.icon_75 + '" style="display: inline-block; width: 35px; height: 35px; border-radius: 100%;">' +
        '<p style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 15px;">' + repo.title + '</p>' +
    '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.title;
}
JS;

                    // Register the formatting script
                    $this->registerJs($formatJs, \yii\web\View::POS_HEAD);


                    // script to parse the results into the format expected by Select2
                    $resultsJs = "
                            function (data, params) {
                                data.items = Object.values(data.items);
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.count
                                    }
                                };
                            }
                            ";
                    ?>
                    <?= $form->field($model, 'appsNot')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Выберите приложения', 'multiple' => true],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => Url::toRoute(['advert/vk-apps-search']),
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) {
                                        return {q:params.term};
                                    }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'minimumInputLength' => 3,
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatRepo'),
                            'templateSelection' => new JsExpression('formatRepoSelection'),
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'travellers')->checkbox() ?>
                </div>
            </div>
            <?php if($model->format == Advert::FORMAT_COMMUNITY_POST && $model->price_type == Advert::PRICE_TYPE_TRANSFER): ?>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'impressions_limit')->dropDownList($model->impressionsLimitList()) ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $formatJs = <<< JS
var formatRepo = function (repo) {
    console.log(repo);
    if (repo.loading) {
        return repo.text;
    }

    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<p style="display: inline-block; margin-left: 5px; margin-bottom: 3px; margin-top: 7px; font-size: 13px;">' + repo.name + '</p>' +
    '</div>' +
'</div>';
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.name;
}
JS;

                    // Register the formatting script
                    $this->registerJs($formatJs, \yii\web\View::POS_HEAD);


                    // script to parse the results into the format expected by Select2
                    $resultsJs = "
                            function (data, params) {
                                data.items = Object.values(data.items);
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.count
                                    }
                                };
                            }
                            ";
                    ?>
                    <?= $form->field($model, 'positions')->widget(Select2::className(), [
                        'options' => ['placeholder' => 'Выберите должности', 'multiple' => true],
                        'pluginOptions' => [
                            'ajax' => [
                                'url' => Url::toRoute(['advert/vk-positions']),
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) {
                                        return {q:params.term};
                                    }'),
                                'processResults' => new JsExpression($resultsJs),
                                'cache' => true
                            ],
                            'minimumInputLength' => 3,
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('formatRepo'),
                            'templateSelection' => new JsExpression('formatRepoSelection'),
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="chck-block">
                        <?= $form->field($model, 'birthday')->radioList(Advert::birthdayLabels()) ?>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="chck-block">
                        <?= $form->field($model, 'sex')->radioList(Advert::sexLabels()) ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="chck-block">
                        <?= $form->field($model, 'family_statuses')->radioList(Advert::familyStatusLabels()) ?>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="chck-block">
                        <?= $form->field($model, 'age_restriction')->radioList(Advert::ageRestrictionLabels()) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="step-wrapper" <?=$model->scenario == Advert::SCENARIO_BIG_IMAGE_NAME ? '' : 'style="display: none;"'?>>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'name')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="hidden">
        <?php if(Advert::getScenarioIndex($model->scenario) > Advert::getScenarioIndex(Advert::SCENARIO_BIG_IMAGE_PHOTO)): ?>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'photo')->textInput() ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="hidden">
        <?= $form->field($model, 'step')->hiddenInput() ?>
        <input type="hidden" name="scenario" value="<?=$model->scenario?>">
        <?= $form->field($model, 'format')->textInput() ?>
    </div>

<?php if (!Yii::$app->request->isAjax){ ?>
    <div class="form-group">
        <?= Html::submitButton($model->isCurrentLastStep() ? 'Создать' : 'Далее', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php } ?>


    </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


<?php

$script = <<< JS
$("#advert-price_type").change(function(){
    var val = parseInt($(this).val());
    console.log(val);
    if(val === 1) {
        $('.cpm-wrapper').show();
        $('.cpc-wrapper').hide();
    } else if(val === 0) {
        $('.cpc-wrapper').show();
        $('.cpm-wrapper').hide();
    }
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>