<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Advert */
/* @var $company app\models\AdvertisingCompany */

$this->title = 'Создание объявления';

?>
<div class="advert-create">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">
                Создание нового объявления
            </h4>
        </div>
        <div class="panel-body">
            <?php if($company->type == \app\models\AdvertisingCompany::TYPE_PROMOTED_POSTS): ?>
                <?= $this->render('_type_promoted_posts', [
                    'model' => $model,
                    'company' => $company,
                ]) ?>
            <?php endif; ?>
            <?php if($company->type == \app\models\AdvertisingCompany::TYPE_NORMAL): ?>
                <?= $this->render('_format_default', [
                    'model' => $model,
                    'company' => $company,
                ]) ?>
            <?php endif; ?>

            <?php

            //            echo $this->render('_form', [
            //                'model' => $model,
            //                'company' => $company,
            //            ])

            ?>
        </div>
    </div>
</div>
