<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Advert */
?>
<div class="advert-view">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Информация</h4>
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'advert_id',
                    'name',
                    'title',
                    'description',
                    'link_url',
                    'company.name',
                    'user.name',
                    'status',
                    'price_type',
                    'price',
                    'day_limit',
                    'general_limit',
                    'ctr',
                    'ecpc',
                    'ecpm',
                    [
                        'attribute' => 'credits',
                        'format' => ['currency', 'rub'],
                    ],
                    [
                        'attribute' => 'views',
                        'format' => 'integer',
                    ],
                    [
                        'attribute' => 'transition',
                        'format' => 'integer'
                    ],
                    [
                        'attribute' => 'last_update_datetime',
                        'format' => ['date', 'php:d M Y H:i:s'],
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => ['date', 'php:d M Y H:i:s'],
                    ],
                ],
            ]) ?>
        </div>
    </div>

</div>
