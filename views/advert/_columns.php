<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\editable\Editable;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\EditableColumn',
        'attribute'=>'name',
        'editableOptions'=> ['formOptions' => ['action' => ['/advert/edit']]]
    ],
    [
        'class'=>'\kartik\grid\EditableColumn',
        'attribute'=>'title',
        'editableOptions'=> ['formOptions' => ['action' => ['/advert/edit']]]
    ],
    [
        'class'=>'\kartik\grid\EditableColumn',
        'attribute'=>'description',
        'editableOptions'=> ['formOptions' => ['action' => ['/advert/edit']]]
    ],
    [
        'class'=>'\kartik\grid\EditableColumn',
        'attribute'=>'link_url',
        'editableOptions'=> ['formOptions' => ['action' => ['/advert/edit']]]
    ],
    [
        'class'=>'\kartik\grid\EditableColumn',
        'attribute'=>'status',
        'content' => function($model){
            return \yii\helpers\ArrayHelper::getValue(\app\models\Advert::statusLabels(), $model->status);
        },
        'editableOptions'=> [
            'formOptions' => ['action' => ['/advert/edit']],
            // 'editableButtonOptions' => [
            'inputType' => Editable::INPUT_DROPDOWN_LIST,
            'data' => \app\models\Advert::statusLabels()
            // ]
        ],
    ],
    [
        'class'=>'\kartik\grid\EditableColumn',
        'attribute'=>'day_limit',
        'editableOptions'=> ['formOptions' => ['action' => ['/advert/edit']]]
    ],
    [
        'class'=>'\kartik\grid\EditableColumn',
        'attribute'=>'general_limit',
        'editableOptions'=> ['formOptions' => ['action' => ['/advert/edit']]]
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'price_type',
    //     'content' => function($model){
    //         return \yii\helpers\ArrayHelper::getValue(\app\models\Advert::priceTypeLabels(), $model->status);
    //     }
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'price',
    // 'format' => ['currency', 'rub'],
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'day_limit',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'general_limit',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'credits',
        'format' => ['currency', 'rub']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'views',
        'format' => 'integer'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'transition',
        'format' => 'integer'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ctr',
        'format' => ['percent', '2']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ecpc',
        'format' => ['currency', 'rub']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'value' => 'user.login',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'ecpm',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'last_update_datetime',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to(["advert/{$action}",'id'=>$key]);
        },
        'template' => '{view} {delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'data-pjax' => 0, 'title'=>'Изменить',
                    ])."&nbsp;";
            }
        ],
    ],

];   