<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\AdvertisingCompany;

/* @var $this yii\web\View */
/* @var $model app\models\AdvertisingCompany */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advertising-company-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList(AdvertisingCompany::statusLabels()) ?>

    <?= $form->field($model, 'type')->dropDownList(AdvertisingCompany::typeLabels()) ?>

    <?= $form->field($model, 'day_limit')->textInput() ?>

    <?= $form->field($model, 'general_limit')->textInput() ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
