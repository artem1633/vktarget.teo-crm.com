<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\AdvertisingCompany;
use kartik\editable\Editable;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        // 'editableOptions'=> ['formOptions' => ['action' => ['/advertising-company/edit']], 'model' => (new AdvertisingCompany()), 'attribute' => 'name'],
        'content' => function($data){
            return Editable::widget([
                'model' => (new AdvertisingCompany(['id' => $data['id']])),
                'attribute' => 'name',
                'displayValue' => $data['name'],
                'value' => $data['name'],
                'formOptions' => ['action' => ['/advertising-company/edit']]
            ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'content' => function($model){
            return \yii\helpers\ArrayHelper::getValue(\app\models\AdvertisingCompany::typeLabels(), $model['type']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content' => function($model){
            return \yii\helpers\ArrayHelper::getValue(\app\models\AdvertisingCompany::statusLabels(), $model['status']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'day_limit',
        'label' => 'Дневной лимит',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'general_limit',
        'label' => 'Общий лимит',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'total_credits',
        'format' => ['currency', 'rub'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'total_views',
        'format' => 'integer',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'total_transition',
        'format' => 'integer',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ctr',
        'format' => ['percent', '2']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ecpc',
        'format' => ['currency', 'rub']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'value' => 'user.login',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$model['id']]);
        },
        'template' => '{view} {delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Изменить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ])."&nbsp;";
            }
        ],
    ],

];   