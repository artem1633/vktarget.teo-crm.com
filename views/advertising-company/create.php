<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AdvertisingCompany */

?>
<div class="advertising-company-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
