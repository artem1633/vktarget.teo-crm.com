<?php

use yii\bootstrap\Modal;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AdvertisingCompany */

\johnitvn\ajaxcrud\CrudAsset::register($this);

$this->title = 'Объявления';

?>
<div class="advertising-company-view">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Информация</h4>
                    <div class="panel-heading-btn" style="margin-top: -20px;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="display: none">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'user_id',
                            'company_id',
                            'name',
                            'status',
                            'day_limit',
                            'general_limit',
                            'created_at',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Объявления</h4>
                </div>
                <div class="panel-body">
                    <?= $this->render('@app/views/advert/index-in-view', [
                        'searchModel' => $advertSearchModel,
                        'dataProvider' => $advertDataProvider,
                        'companyId' => $model->id,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
