<?php

use yii\db\Migration;

/**
 * Handles adding ages to table `advert`.
 */
class m191026_160814_add_ages_columns_to_advert_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advert', 'age_from', $this->string()->after('video')->comment('Возраст начала'));
        $this->addColumn('advert', 'age_to', $this->string()->after('age_from')->comment('Возраст конца'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('advert', 'age_from');
        $this->dropColumn('advert', 'age_to');
    }
}
