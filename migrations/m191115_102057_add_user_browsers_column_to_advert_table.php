<?php

use yii\db\Migration;

/**
 * Handles adding user_browsers to table `advert`.
 */
class m191115_102057_add_user_browsers_column_to_advert_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advert', 'user_browsers', $this->string()->comment('Интернет-браузеры'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('advert', 'user_browsers');
    }
}
