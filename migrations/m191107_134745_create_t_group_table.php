<?php

use yii\db\Migration;

/**
 * Handles the creation of table `t_group`.
 */
class m191107_134745_create_t_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('t_group', [
            'id' => $this->primaryKey(),
            'advert_id' => $this->integer()->comment('Объявление'),
            'vk_id' => $this->integer()->comment('ID группы'),
            'title' => $this->string()->comment('Наименование группы'),
        ]);

        $this->createIndex(
            'idx-t_group-advert_id',
            't_group',
            'advert_id'
        );

        $this->addForeignKey(
            'fk-t_group-advert_id',
            't_group',
            'advert_id',
            'advert',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-t_group-advert_id',
            't_group'
        );

        $this->dropIndex(
            'idx-t_group-advert_id',
            't_group'
        );

        $this->dropTable('t_group');
    }
}
