<?php

use yii\db\Migration;

/**
 * Handles adding ad_platform to table `advert`.
 */
class m191115_094113_add_ad_platform_column_to_advert_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advert', 'ad_platform', $this->string()->comment('Рекламные площадки, на которых будет показываться объявление'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('advert', 'ad_platform');
    }
}
