<?php

use yii\db\Migration;

/**
 * Handles adding age_restriction to table `advert`.
 */
class m191107_164930_add_age_restriction_column_to_advert_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advert', 'age_restriction', $this->integer()->comment('Отображение отметки возрастного ограничения на объявлении'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('advert', 'age_restriction');
    }
}
