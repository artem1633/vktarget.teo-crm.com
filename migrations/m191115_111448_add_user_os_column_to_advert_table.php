<?php

use yii\db\Migration;

/**
 * Handles adding user_os to table `advert`.
 */
class m191115_111448_add_user_os_column_to_advert_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advert', 'user_os', $this->string()->comment('Операционные системы'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('advert', 'user_os');
    }
}
