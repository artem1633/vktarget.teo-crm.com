<?php

use yii\db\Migration;

/**
 * Class m191022_125713_add_account_id_setting
 */
class m191022_125713_add_account_id_setting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'account_id',
            'label' => 'ID рекламного кабинета'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => 'account_id']);
    }
}
