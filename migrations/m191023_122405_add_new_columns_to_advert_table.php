<?php

use yii\db\Migration;

/**
 * Handles adding new to table `advert`.
 */
class m191023_122405_add_new_columns_to_advert_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advert', 'title', $this->string()->after('name')->comment('Заголовок'));
        $this->addColumn('advert', 'format', $this->integer()->after('title')->comment('Формат'));
        $this->addColumn('advert', 'cpc', $this->float()->after('format')->comment('CPC'));
        $this->addColumn('advert', 'cpm', $this->float()->after('cpc')->comment('CPM'));
        $this->addColumn('advert', 'ocpm', $this->float()->after('cpm')->comment('OCPM'));
        $this->addColumn('advert', 'goal_type', $this->integer()->after('ocpm')->comment('Тип цели'));
        $this->addColumn('advert', 'impressions_limit', $this->integer()->after('goal_type')->comment('Ограничение показов'));
        $this->addColumn('advert', 'impressions_limited', $this->integer()->after('impressions_limit')->comment('Признак того, что количество показов объявления на одного пользователя ограничено'));
        $this->addColumn('advert', 'platform', $this->integer()->after('impressions_limited')->comment('Рекламные площадки'));
        $this->addColumn('advert', 'ad_platform_no_wall', $this->integer()->after('platform')->comment('Показывать также на стенах сообществ'));
        $this->addColumn('advert', 'ad_platform_no_ad_network', $this->integer()->after('ad_platform_no_wall')->comment('Показывать в рекламной сети'));
        $this->addColumn('advert', 'category1_id', $this->integer()->after('ad_platform_no_ad_network')->comment('Интересы и поведение'));
        $this->addColumn('advert', 'category2_id', $this->integer()->after('category1_id')->comment('Интересы и поведение'));
        $this->addColumn('advert', 'description', $this->text()->after('title')->comment('Описание'));
        $this->addColumn('advert', 'link_url', $this->string()->after('category2_id')->comment('Ссылка'));
        $this->addColumn('advert', 'link_domain', $this->string()->after('link_url')->comment('Домен'));
        $this->addColumn('advert', 'link_title', $this->string()->after('link_domain')->comment('Заголовок ссылки'));
        $this->addColumn('advert', 'link_button', $this->string()->after('link_title')->comment('Кнопка'));
        $this->addColumn('advert', 'photo', $this->string()->after('link_button')->comment('Изображение'));
        $this->addColumn('advert', 'photo_icon', $this->string()->after('photo')->comment('Логотип'));
        $this->addColumn('advert', 'video', $this->string()->after('photo_icon')->comment('Видео'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('advert', 'title');
        $this->dropColumn('advert', 'format');
        $this->dropColumn('advert', 'cpc');
        $this->dropColumn('advert', 'cpm');
        $this->dropColumn('advert', 'ocpm');
        $this->dropColumn('advert', 'goal_type');
        $this->dropColumn('advert', 'impressions_limit');
        $this->dropColumn('advert', 'impressions_limited');
        $this->dropColumn('advert', 'platform');
        $this->dropColumn('advert', 'ad_platform_no_wall');
        $this->dropColumn('advert', 'ad_platform_no_ad_network');
        $this->dropColumn('advert', 'category1_id');
        $this->dropColumn('advert', 'category2_id');
        $this->dropColumn('advert', 'description');
        $this->dropColumn('advert', 'link_url');
        $this->dropColumn('advert', 'link_domain');
        $this->dropColumn('advert', 'link_title');
        $this->dropColumn('advert', 'link_button');
        $this->dropColumn('advert', 'photo');
        $this->dropColumn('advert', 'photo_icon');
        $this->dropColumn('advert', 'video');
    }
}
