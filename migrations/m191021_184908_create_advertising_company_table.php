<?php

use yii\db\Migration;

/**
 * Handles the creation of table `advertising_company`.
 */
class m191021_184908_create_advertising_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('advertising_company', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'name' => $this->string()->comment('Наименование'),
            'status' => $this->integer()->comment('Статус'),
            'day_limit' => $this->integer()->comment('Дневной лимит на траты (на клик)'),
            'general_limit' => $this->integer()->comment('Общий лимит (на клик)'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-advertising_company-user_id',
            'advertising_company',
            'user_id'
        );

        $this->addForeignKey(
            'fk-advertising_company-user_id',
            'advertising_company',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-advertising_company-user_id',
            'advertising_company'
        );

        $this->dropIndex(
            'idx-advertising_company-user_id',
            'advertising_company'
        );

        $this->dropTable('advertising_company');
    }
}
