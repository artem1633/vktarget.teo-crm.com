<?php

use yii\db\Migration;

/**
 * Handles adding company_id to table `advertising_company`.
 */
class m191022_150146_add_company_id_column_to_advertising_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advertising_company', 'company_id', $this->string()->comment('Внешний ID ВК'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('advertising_company', 'company_id');
    }
}
