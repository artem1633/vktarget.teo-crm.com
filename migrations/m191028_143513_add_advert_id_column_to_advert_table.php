<?php

use yii\db\Migration;

/**
 * Handles adding advert_id to table `advert`.
 */
class m191028_143513_add_advert_id_column_to_advert_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advert', 'advert_id', $this->integer()->comment('Внешний ID'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('advert', 'advert_id');
    }
}
