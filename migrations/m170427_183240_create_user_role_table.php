<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_role`.
 */
class m170427_183240_create_user_role_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_role', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'max_month_credits' => $this->float()->comment('Предельный объем трат за 1 календарный мес'),
            'max_day_credits' => $this->float()->comment('Предельный объем трат за 1 день'),
            'max_rate_click' => $this->float()->comment('Предельная ставка за 1 клик'),
            'max_thousand_rate_view' => $this->float()->comment('Предельная ставка за 1000 показов'),
            'max_credit_speed_in_value' => $this->float()->comment('Предельная скорость трат в рублях'),
            'max_credit_speed_in_min' => $this->float()->comment('Предельная скорость трат в минутах'),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_role');
    }
}
