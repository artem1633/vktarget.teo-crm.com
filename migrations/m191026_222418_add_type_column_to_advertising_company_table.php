<?php

use yii\db\Migration;

/**
 * Handles adding type to table `advertising_company`.
 */
class m191026_222418_add_type_column_to_advertising_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advertising_company', 'type', $this->string()->after('status')->comment('Тип'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('advertising_company', 'type');
    }
}
