<?php

use yii\db\Migration;

/**
 * Handles adding positions to table `advert`.
 */
class m191115_120940_add_positions_column_to_advert_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advert', 'positions', $this->integer()->comment('Должности'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('advert', 'positions');
    }
}
