<?php

use yii\db\Migration;

/**
 * Handles adding targeting to table `advert`.
 */
class m191106_164542_add_targeting_columns_to_advert_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advert', 'sex', $this->integer()->comment('Пол'));
        $this->addColumn('advert', 'birthday', $this->integer()->comment('День рождения'));
        $this->addColumn('advert', 'country', $this->integer()->comment('Страна'));
        $this->addColumn('advert', 'cities', $this->string()->comment('Города'));
        $this->addColumn('advert', 'cities_not', $this->string()->comment('Города исключения'));
        $this->addColumn('advert', 'family_statuses', $this->string()->comment('Семейное положение'));
        $this->addColumn('advert', 'groups', $this->string()->comment('Группы'));
        $this->addColumn('advert', 'groups_not', $this->string()->comment('Группы исключения'));
        $this->addColumn('advert', 'groups_active', $this->string()->comment('Группы где клиент проявлял активность'));
        $this->addColumn('advert', 'schools', $this->string()->comment('Ids УЗ'));
        $this->addColumn('advert', 'religions', $this->string()->comment('Религиозные взгляды'));
        $this->addColumn('advert', 'interest_categories', $this->string()->comment('Интересы'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
