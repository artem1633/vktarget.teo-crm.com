<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m191021_191435_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique()->comment('Ключ'),
            'value' => $this->string()->comment('Значение'),
            'label' => $this->text()->comment('Комментарий'),
        ]);
        $this->addCommentOnTable('settings', 'Настройки системы');

        $this->insert('settings', [
            'key' => 'interval_update',
            'label' => 'Интервал обновления (в мин.)'
        ]);

        $this->insert('settings', [
            'key' => 'vk_token',
            'label' => 'ВК токен',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings');
    }
}
