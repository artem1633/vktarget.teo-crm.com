<?php

use yii\db\Migration;

/**
 * Handles the creation of table `advert`.
 */
class m191021_185903_create_advert_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('advert', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'company_id' => $this->integer()->comment('Компания'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'status' => $this->integer()->comment('Статус'),
            'price_type' => $this->integer()->comment('Тип цены'),
            'price' => $this->float()->comment('Цена'),
            'day_limit' => $this->integer()->comment('Дневной лимит на траты (на клик)'),
            'general_limit' => $this->integer()->comment('Общий лимит (на клик)'),
            'credits' => $this->float()->comment('Потрачено'),
            'views' => $this->integer()->comment('Кол-во показов'),
            'transition' => $this->integer()->comment('Кол-во переходов'),
            'ctr' => $this->float()->comment('CTR'),
            'ecpc' => $this->float()->comment('eCPC'),
            'ecpm' => $this->float()->comment('eCPM'),
            'last_update_datetime' => $this->dateTime()->comment('Дата и время последнего обновления'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-advert-company_id',
            'advert',
            'company_id'
        );

        $this->addForeignKey(
            'fk-advert-company_id',
            'advert',
            'company_id',
            'advertising_company',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-advert-user_id',
            'advert',
            'user_id'
        );

        $this->addForeignKey(
            'fk-advert-user_id',
            'advert',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-advert-company_id',
            'advert'
        );

        $this->dropIndex(
            'idx-advert-company_id',
            'advert'
        );

        $this->dropForeignKey(
            'fk-advert-user_id',
            'advert'
        );

        $this->dropIndex(
            'idx-advert-user_id',
            'advert'
        );

        $this->dropTable('advert');
    }
}
