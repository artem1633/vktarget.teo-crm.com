<?php

use yii\db\Migration;

/**
 * Handles the creation of table `t_city`.
 */
class m191107_123609_create_t_city_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('t_city', [
            'id' => $this->primaryKey(),
            'advert_id' => $this->integer()->comment('Объявление'),
            'vk_id' => $this->integer()->comment('ID города'),
            'title' => $this->string()->comment('Наименование города'),
        ]);

        $this->createIndex(
            'idx-t_city-advert_id',
            't_city',
            'advert_id'
        );

        $this->addForeignKey(
            'fk-t_city-advert_id',
            't_city',
            'advert_id',
            'advert',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-t_city-advert_id',
            't_city'
        );

        $this->dropIndex(
            'idx-t_city-advert_id',
            't_city'
        );

        $this->dropTable('t_city');
    }
}
