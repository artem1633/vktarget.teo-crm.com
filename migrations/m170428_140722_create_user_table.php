<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170428_140722_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull()->comment('Логин'),
            'name' => $this->string()->comment('ФИО'),
            'password_hash' => $this->string()->notNull()->comment('Зашифрованный пароль'),
            'role_id' => $this->integer()->comment('Роль'),
            'permission' => $this->integer()->comment('Права'),
            'is_deletable' => $this->boolean()->notNull()->defaultValue(true)->comment('Можно удалить или нельзя'),
            'created_at' => $this->dateTime(),
        ]);

        $this->insert('user', [
            'login' => 'admin',
            'permission' => \app\models\User::PERMISSION_ADMIN,
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'is_deletable' => false,
        ]);

        $this->createIndex(
            'idx-user-role_id',
            'user',
            'role_id'
        );

        $this->addForeignKey(
            'fk-user-role_id',
            'user',
            'role_id',
            'user_role',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-user-role_id',
            'user'
        );

        $this->dropIndex(
            'idx-user-role_id',
            'user'
        );

        $this->dropTable('user');
    }
}
