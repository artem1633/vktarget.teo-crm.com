<?php

use yii\db\Migration;

/**
 * Handles adding user_devices to table `advert`.
 */
class m191115_111601_add_user_devices_column_to_advert_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('advert', 'user_devices', $this->string()->comment('Устройства'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('advert', 'user_devices');
    }
}
