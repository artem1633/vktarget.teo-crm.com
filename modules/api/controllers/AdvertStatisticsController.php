<?php

namespace app\modules\api\controllers;

use app\components\helpers\StatisticCalculation;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use app\components\VkAPI;
use app\models\Advert;
use app\models\Settings;

use yii\helpers\VarDumper;

/**
 * Class AdvertStatisticsController
 * @package app\modules\api\controllers
 */
class AdvertStatisticsController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['save-statistics'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    /**
     * @return mixed
     */
    public function actionSaveStatistics()
    {
        $accountId = Settings::findByKey('account_id')->value;

        $hours24Update = date('Y-m-d H:i:s', time() - 120); // 120 — 2 минуты

        /** @var Advert[] $adverts */
        $adverts = Advert::find()->where(['is not', 'advert_id', null])->andWhere(['or', ['<', 'last_update_datetime', $hours24Update], ['last_update_datetime' => null]])->all();

        $response = [];

        foreach ($adverts as $advert){

            $res = VkAPI::getStatistics($accountId, $advert);
            $response[] = $res;

            if(isset($res['response'][0]['stats'])){
                if(count($res['response'][0]['stats']) > 0){
                    $stat = $res['response'][0]['stats'][0];
                    $advert->views = $stat['impressions'];
                    $advert->transition = $stat['clicks'];
                    $advert->credits = $stat['spent'];
                    $advert->ctr = StatisticCalculation::calculateCTR($stat['impressions'], $stat['clicks']);
                    $advert->ecpc = StatisticCalculation::calculateECPC($stat['spent'], $stat['clicks']);
                }
            }

            $advert->last_update_datetime = date('Y-m-d H:i:s');
            $advert->save(false);

        }

        return $response;
    }
}