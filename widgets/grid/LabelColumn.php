<?php

/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 21.10.2019
 * Time: 22:56
 */
class LabelColumn extends \yii\grid\DataColumn
{
    /**
     * @var array
     */
    public $labels;

    protected function renderDataCellContent($model, $key, $index)
    {
        return parent::renderDataCellContent($model, $key, $index);
    }
}