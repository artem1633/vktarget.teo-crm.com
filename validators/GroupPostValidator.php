<?php

namespace app\validators;

use app\models\Advert;
use yii\validators\Validator;

/**
 * Class GroupPostValidator
 * @package app\validators
 */
class GroupPostValidator extends Validator
{
    public $message = "Данная ссылка уже используется в другом объявлении";

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        $groupPost = $model->$attribute;

        $advert = Advert::find()->where(['format' => Advert::FORMAT_COMMUNITY_POST, 'link_url' => $groupPost])->one();

        if($advert) {
            $this->addError($model, $attribute, $this->message);
        }
    }
}