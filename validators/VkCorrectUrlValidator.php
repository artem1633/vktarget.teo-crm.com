<?php

namespace app\validators;

use yii\validators\Validator;
use app\components\VkAPI;

/**
 * Class VkCorrectUrlValidator
 * @package app\validators
 */
class VkCorrectUrlValidator extends Validator
{
    public $message = "Ссылка некорректная";

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        $url = $model->$attribute;

        $group = explode('/', $url);

        if(count($group) == 0){
            $this->addError($model, $attribute, $this->message);
            return false;
        }

        $group = $group[count($group)-1];

        $response = VkAPI::resolveScreenName($group);

        if(count($response['response']) == 0){
            $this->addError($model, $attribute, $this->message);
            return false;
        } elseif($response['response']['type'] != 'group') {
            $this->addError($model, $attribute, $this->message);
            return false;
        }
    }
}