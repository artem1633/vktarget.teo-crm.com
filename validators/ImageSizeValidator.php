<?php

namespace app\validators;

use yii\validators\Validator;
use yii\web\UploadedFile;

/**
 * Class ImageSizeValidator
 * @package app\validators
 */
class ImageSizeValidator extends Validator
{
    /**
     * @var integer
     */
    public $maxWidth;

    /**
     * @var integer
     */
    public $maxHeight;

    /**
     * @var integer
     */
    public $minWidth;

    /**
     * @var integer
     */
    public $minHeight;

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        /** @var UploadedFile $photo */
        $photo = $model->$attribute;

        if($photo instanceof UploadedFile == false){
            return;
        }

        list($width, $height, $type, $attr) = getimagesize($photo->tempName);

        if($this->maxWidth != null && $width > $this->maxWidth){
            $this->addError($model, $attribute, "Максимальная ширина изображения {$this->maxWidth}px");
            return false;
        }

        if($this->maxHeight != null && $height > $this->maxHeight){
            $this->addError($model, $attribute, "Максимальная высота изображения {$this->maxHeight}px");
            return false;
        }

        if($this->minWidth != null && $height < $this->minWidth){
            $this->addError($model, $attribute, "Минимальная ширина изображения {$this->minWidth}px");
            return false;
        }

        if($this->minHeight != null && $height < $this->minHeight){
            $this->addError($model, $attribute, "Минимальная высота изображения {$this->minHeight}px");
            return false;
        }
    }
}