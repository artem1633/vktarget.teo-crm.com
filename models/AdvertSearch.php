<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Advert;

/**
 * AdvertSearch represents the model behind the search form about `app\models\Advert`.
 */
class AdvertSearch extends Advert
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'user_id', 'status', 'price_type', 'day_limit', 'general_limit', 'views', 'transition'], 'integer'],
            [['name', 'last_update_datetime', 'created_at'], 'safe'],
            [['price', 'credits', 'ctr', 'ecpc', 'ecpm'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Advert::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'price_type' => $this->price_type,
            'price' => $this->price,
            'day_limit' => $this->day_limit,
            'general_limit' => $this->general_limit,
            'credits' => $this->credits,
            'views' => $this->views,
            'transition' => $this->transition,
            'ctr' => $this->ctr,
            'ecpc' => $this->ecpc,
            'ecpm' => $this->ecpm,
            'last_update_datetime' => $this->last_update_datetime,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
