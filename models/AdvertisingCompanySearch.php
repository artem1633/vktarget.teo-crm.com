<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AdvertisingCompany;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * AdvertisingCompanySearch represents the model behind the search form about `app\models\AdvertisingCompany`.
 *
 * @property double $total_credits
 * @property integer $total_transition
 * @property integer $total_views
 * @property double $total_ctr
 * @property double $total_ecpc
 */
class AdvertisingCompanySearch extends AdvertisingCompany
{
    /**
     * @var double
     */
    public $total_credits;

    /**
     * @var integer
     */
    public $total_transition;

    /**
     * @var integer
     */
    public $total_views;

    /**
     * @var double
     */
    public $total_ctr;

    /**
     * @var double
     */
    public $total_ecpc;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['total_credits', 'total_ctr', 'total_ecpc'], 'number'],
            [['id', 'user_id', 'status', 'day_limit', 'general_limit', 'total_transition', 'total_views', 'type'], 'integer'],
            [['name', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new Query())->select('advertising_company.*, SUM(advert.credits) as total_credits, SUM(advert.views) as total_views,
         SUM(advert.transition) as total_transition, AVG(advert.ctr) as ctr, AVG(advert.ecpc) as ecpc')->from('advertising_company');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
//            'sort' => [
//                'defaultOrder' => [
//                    'id' => SORT_DESC,
//                ],
//            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->leftJoin('advert', 'advert.company_id = advertising_company.id');

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'day_limit' => $this->day_limit,
            'general_limit' => $this->general_limit,
            'total_credits' => $this->total_credits,
            'total_transition' => $this->total_transition,
            'total_views' => $this->total_views,
            'created_at' => $this->created_at,
        ]);

        $query->groupBy('advertising_company.id');

        $query->andFilterWhere(['like', 'name', $this->name]);

        $query->orderBy('id desc');

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'total_credits' => 'Всего потрачено',
            'total_views' => 'Всего просмотров',
            'total_transition' => 'Всего переходов',
            'ctr' => 'CTR',
            'ecpc' => 'eCPC'
        ]);
    }
}
