<?php

namespace app\models;

use app\components\VkAPI;
use Yii;

/**
 * This is the model class for table "t_city".
 *
 * @property int $id
 * @property int $advert_id Объявление
 * @property int $vk_id ID города
 * @property string $title Наименование города
 *
 * @property Advert $advert
 */
class TCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['advert_id', 'vk_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['advert_id'], 'exist', 'skipOnError' => true, 'targetClass' => Advert::className(), 'targetAttribute' => ['advert_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'advert_id' => 'Объявление',
            'vk_id' => 'ID города',
            'title' => 'Наименование города',
        ];
    }

    /**
     * @param integer $advertId
     * @param array $ids
     * @return boolean
     */
    public static function createFromAdvert($advertId, $ids)
    {
        if(count($ids) > 1000){
            return false;
        }

        $cities = VkAPI::getCitiesById($ids);

        if(isset($cities['response'])){
            foreach ($cities['response'] as $item){
                $city = new TCity([
                    'advert_id' => $advertId,
                    'vk_id' => $item['id'],
                    'title' => $item['title']
                ]);
                $city->save(false);
            }
            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvert()
    {
        return $this->hasOne(Advert::className(), ['id' => 'advert_id']);
    }
}
