<?php

namespace app\models;

use app\components\VkAPI;
use app\validators\GroupPostValidator;
use app\validators\VkCorrectUrlValidator;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use app\validators\ImageSizeValidator;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "advert".
 *
 * @property int $id
 * @property string $name Наименование
 * @property int $company_id Компания
 * @property int $user_id Пользователь
 * @property int $status Статус
 * @property string $photo Основное изображение
 * @property string $photo_icon Иконка
 * @property int $price_type Тип цены
 * @property double $price Цена
 * @property int $day_limit Дневной лимит на траты (на клик)
 * @property int $general_limit Общий лимит (на клик)
 * @property double $credits Потрачено
 * @property int $views Кол-во показов
 * @property int $transition Кол-во переходов
 * @property double $ctr CTR
 * @property double $ecpc eCPC
 * @property double $ecpm eCPM
 * @property string $last_update_datetime Дата и время последнего обновления
 * @property string $age_from Возраст от
 * @property string $age_to Возраст до
 * @property string $created_at
 * @property integer $advert_id
 * @property string $user_browsers
 * @property string $user_os
 * @property string $user_devices
 * @property string $positions
 * @property integer $impressions_limit
 *
 * @property AdvertisingCompany $company
 * @property User $user
 */
class Advert extends \yii\db\ActiveRecord
{
    const STATUS_STOPPED = 0;
    const STATUS_ACTIVE = 1;

    const PRICE_TYPE_TRANSFER = 0;
    const PRICE_TYPE_VIEW = 1;

    const FORMAT_IMAGE_TEXT = 1;
    const FORMAT_BIG_IMAGE = 2;
    const FORMAT_COMMUNITY = 4;
    const FORMAT_SPEC_COMMUNITY = 8;
    const FORMAT_COMMUNITY_POST = 9;
    const FORMAT_ADAPTIVE = 11;

    const GOAL_TYPE_VIEWS = 1;
    const GOAL_TYPE_TRANSFER = 2;
    const GOAL_TYPE_SEND = 3;

    const SCENARIO_FORMAT = 'scenario_format';
    const SCENARIO_LINK = 'scenario_link';
    const SCENARIO_PRICES = 'scenario_price';
    const SCENARIO_TARGETING = 'scenario_targeting';
    const SCENARIO_NAME = 'scenario_name';

    const SCENARIO_TITLE = 'scenario_title';
    const SCENARIO_PHOTO = 'scenario_file';
    const SCENARIO_PRICE_TRANSFER = 'scenario_price_transfer';
    const SCENARIO_IMAGE_TEXT_TARGETING = 'scenario_image_text_targeting';
    const SCENARIO_IMAGE_TEXT_NAME = 'scenario_image_text_name';

    const SCENARIO_BIG_IMAGE_TITLE = 'scenario_big_image_title';
    const SCENARIO_BIG_IMAGE_PHOTO = 'scenario_big_image_file';
    const SCENARIO_BIG_IMAGE_PRICE_TRANSFER = 'scenario_big_image_price_transfer';
    const SCENARIO_BIG_IMAGE_TARGETING = 'scenario_big_image_targeting';
    const SCENARIO_BIG_IMAGE_NAME = 'scenario_big_image_name';

    const SCENARIO_COMMUNITY_LINK = 'scenario_community_link';
    const SCENARIO_COMMUNITY_PHOTO = 'scenario_community_photo';
    const SCENARIO_COMMUNITY_PRICE_TRANSFER = 'scenario_community_price_transfer';
    const SCENARIO_COMMUNITY_TARGETING = 'scenario_community_targeting';
    const SCENARIO_COMMUNITY_NAME = 'scenario_community_name';

    /**
     * @param integer
     */
    public $step;


    public $targetGroups;

    public $targetGroupsNot;

    public $schools;

    public $unies;

    public $apps;

    public $appsNot;

    public $travellers;

    public $eventsRetargetingGroupsGroup;

    public $eventsRetargetingGroupsEvent;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advert';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => null,
                'createdByAttribute' => 'user_id',
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $targetingAttributes = ['age_from', 'age_to', 'country', 'cities', 'cities_not', 'groups', 'groups_not', 'groups_active', 'birthday', 'sex', 'family_statuses', 'age_restriction', 'category1_id', 'category2_id', 'ad_platform', 'user_browsers', 'user_os', 'user_devices', 'positions', 'impressions_limit', 'targetGroups', 'targetGroupsNot', 'schools', 'apps', 'appsNot', 'travellers', 'eventsRetargetingGroupsGroup', 'eventsRetargetingGroupsEvent', 'unies'];

        return [
            self::SCENARIO_FORMAT => ['scenario', 'step', 'format'],

            self::SCENARIO_LINK => ['scenario', 'step', 'goal_type', 'format', 'link_url'],
            self::SCENARIO_PRICES => ['scenario', 'step', 'goal_type', 'format', 'link_url', 'price_type', 'cpc', 'cpm'],
            self::SCENARIO_TARGETING => ArrayHelper::merge($targetingAttributes, ['scenario', 'step', 'goal_type', 'format', 'link_url', 'price_type', 'cpc', 'cpm']),
            self::SCENARIO_NAME => ArrayHelper::merge($targetingAttributes, ['scenario', 'step', 'goal_type', 'format', 'link_url', 'price_type', 'cpc', 'cpm', 'name', 'status', 'day_limit', 'general_limit']),

            self::SCENARIO_TITLE => ['scenario', 'step', 'format', 'title', 'description', 'link_url', 'goal_type'],
            self::SCENARIO_PHOTO => ['scenario', 'step', 'format', 'title', 'description', 'link_url', 'goal_type', 'photo'],
            self::SCENARIO_PRICE_TRANSFER => ['scenario', 'step', 'format', 'title', 'description', 'link_url', 'goal_type', 'photo', 'price_type', 'cpc'],
            self::SCENARIO_IMAGE_TEXT_TARGETING => ArrayHelper::merge($targetingAttributes, ['scenario', 'step', 'format', 'title', 'description', 'link_url', 'goal_type', 'photo', 'price_type', 'cpc']),
            self::SCENARIO_IMAGE_TEXT_NAME => ArrayHelper::merge($targetingAttributes, ['scenario', 'step', 'format', 'title', 'description', 'link_url', 'goal_type', 'photo', 'price_type', 'cpc', 'name', 'status', 'day_limit', 'general_limit']),

            self::SCENARIO_BIG_IMAGE_TITLE => ['scenario', 'step', 'format', 'title', 'link_url', 'goal_type'],
            self::SCENARIO_BIG_IMAGE_PHOTO => ['scenario', 'step', 'format', 'title', 'link_url', 'goal_type', 'photo'],
            self::SCENARIO_BIG_IMAGE_PRICE_TRANSFER => ['scenario', 'step', 'format', 'title', 'link_url', 'goal_type', 'photo', 'price_type', 'cpc'],
            self::SCENARIO_BIG_IMAGE_TARGETING => ArrayHelper::merge($targetingAttributes, ['scenario', 'step', 'format', 'title', 'link_url', 'goal_type', 'photo', 'price_type', 'cpc']),
            self::SCENARIO_BIG_IMAGE_NAME => ArrayHelper::merge($targetingAttributes, ['scenario', 'step', 'format', 'title', 'link_url', 'goal_type', 'photo', 'price_type', 'cpc', 'name', 'status', 'day_limit', 'general_limit']),

            self::SCENARIO_COMMUNITY_LINK => ['scenario', 'step', 'format', 'link_url', 'goal_type'],
            self::SCENARIO_COMMUNITY_PHOTO => ['scenario', 'step', 'format', 'link_url', 'goal_type', 'photo'],
            self::SCENARIO_COMMUNITY_PRICE_TRANSFER => ['scenario', 'step', 'format', 'link_url', 'goal_type', 'photo', 'price_type', 'cpc'],
            self::SCENARIO_COMMUNITY_TARGETING => ArrayHelper::merge($targetingAttributes, ['scenario', 'step', 'format', 'link_url', 'goal_type', 'photo', 'price_type', 'cpc']),
            self::SCENARIO_COMMUNITY_NAME => ArrayHelper::merge($targetingAttributes, ['scenario', 'step', 'format', 'link_url', 'goal_type', 'photo', 'price_type', 'cpc', 'name', 'status', 'day_limit', 'general_limit']),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'ocpm', 'age_from', 'age_to', 'link_url'], 'required'],
            [['title', 'description'], 'required', 'when' => function($model){
                return in_array($model->format, [self::FORMAT_IMAGE_TEXT]);
            }],


            ['price_type', 'default', 'value' => self::PRICE_TYPE_VIEW, 'when' => function($model){
                $model->format == self::FORMAT_COMMUNITY_POST;
            }],
            [['price_type'], 'required', 'when' => function($model){
                return $model->format != self::FORMAT_COMMUNITY_POST;
            }],

            [['link_url'], GroupPostValidator::className(), 'when' => function($model){
                return in_array($model->format, [self::FORMAT_COMMUNITY_POST]);
            }],

            ['link_url', VkCorrectUrlValidator::className(), 'on' => self::SCENARIO_COMMUNITY_LINK],

            ['link_url', function(){
                if(strstr($this->link_url, 'vk') == false){
                    $this->addError('link_url', 'Некорректная ссылка');
                    return false;
                }
            }, 'on' => self::SCENARIO_LINK],

            ['link_url', 'match',
                'pattern' => '/\b((http|https):\/\/?)[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/?))/i',
                'on' => [self::SCENARIO_TITLE, self::SCENARIO_IMAGE_TEXT_NAME],
                'message' => 'Некорректная ссылка'],

            [['company_id', 'user_id', 'status', 'price_type', 'day_limit', 'general_limit', 'views', 'transition', 'format', 'goal_type', 'advert_id', 'impressions_limit'], 'integer'],
            [['price', 'credits', 'ctr', 'ecpc', 'ecpm', 'cpc', 'cpm', 'ocpm', 'step'], 'number'],
            [['last_update_datetime', 'created_at'], 'safe'],
            [['name', 'link_url', 'title', 'description', 'ad_platform'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdvertisingCompany::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],

            // ['photo', 'file'],

            ['photo_icon', 'file'],

            ['photo', 'required', 'on' => [self::SCENARIO_PHOTO, self::SCENARIO_BIG_IMAGE_PHOTO]],
            ['photo', 'file', 'extensions' => 'jpg, png, bmp, tif, gif', 'maxSize' => (1024*1024)*5, 'on' => [self::SCENARIO_PHOTO, self::SCENARIO_BIG_IMAGE_PHOTO]],
            ['photo', ImageSizeValidator::className(), 'minWidth' => 145, 'minHeight' => 85, 'on' => self::SCENARIO_PHOTO],
            ['photo', ImageSizeValidator::className(), 'minWidth' => 145, 'minHeight' => 165, 'on' => self::SCENARIO_BIG_IMAGE_PHOTO],

            ['cpc', 'required', 'when' => function($model){
                return $model->price_type == self::PRICE_TYPE_TRANSFER;
            }],

            ['cpm', 'required', 'when' => function($model){
                return $model->price_type == self::PRICE_TYPE_VIEW || $model->format == self::FORMAT_COMMUNITY_POST;
            }],

            ['ocpm', 'number', 'skipOnEmpty' => true, 'min' => 2],

            ['cpc', 'number', 'skipOnEmpty' => true, 'min' => 6],

            [['cpm'], 'number', 'min' => 30, 'skipOnEmpty' => true,'when' => function($model){
                return in_array($model->format, [self::FORMAT_COMMUNITY_POST]);
            }],

            [['cpm'], 'number', 'max' => 20, 'skipOnEmpty' => true, 'when' => function($model){
                return in_array($model->format, [self::FORMAT_IMAGE_TEXT]);
            }],

            ['status', function(){
                if($this->company->status == 0 && $this->status == 1){
                    $this->addError('status', 'Вы не можете запустить рекламу, пока компания не запущена');
                    return false;
                }
            }],

            [['day_limit', 'general_limit'], 'number', 'min' => 100],

            ['age_from', 'integer', 'min' => 14, 'max' => 80],
            ['age_to', 'integer', 'min' => 14, 'max' => 80],

            ['age_restriction', function(){
                $age = self::ageRestrictionYears()[$this->age_restriction];
                if($age > $this->age_from){
                    $attrLabel = $this->attributeLabels()['age_from'];
                    $this->addError('age_restriction', "Возрастное ограничение не может быть меньше значения в поле «{$attrLabel}»");
                    return false;
                }
                return false;
            }],

            [['cities', 'cities_not', 'groups', 'groups_not', 'grous_active', 'user_os', 'user_devices', 'positions', 'targetGroups', 'targetGroupsNot', 'schools'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'company_id' => 'Компания',
            'user_id' => 'Пользователь',
            'status' => 'Статус',
            'price_type' => 'Тип цены',
            'format' => 'Формат',
            'photo' => 'Основное изображение',
            'photo_icon' => 'Иконка',
            'goal_type' => 'Цель',
            'price' => 'Цена',
            'link_url' => 'Ссылка',
            'day_limit' => 'Дневной лимит на траты (на клик)',
            'general_limit' => 'Общий лимит (на клик)',
            'credits' => 'Потрачено',
            'views' => 'Кол-во показов',
            'transition' => 'Кол-во переходов',
            'cpc' => 'Цена за переход',
            'cpm' => 'Цена за 1000 показов',
            'ocpm' => 'Цена за действие для oCPM',
            'ctr' => 'CTR',
            'ecpc' => 'eCPC',
            'ecpm' => 'eCPM',
            'birthday' => 'День рождения',
            'sex' => 'Пол',
            'family_statuses' => 'Семейное положение',
            'last_update_datetime' => 'Дата и время последнего обновления',
            'age_from' => 'Возраст (от)',
            'age_to' => 'Возраст (до)',
            'advert_id' => 'Внешний ID',
            'country' => 'Страна',
            'cities' => 'Города',
            'cities_not' => 'Города исключения',
            'groups' => 'Сообщества',
            'groups_not' => 'Сообщества исключения',
            'groups_active' => 'Сообщества в которых активность',
            'category1_id' => 'Тематика',
            'category2_id' => 'Дополнительная тематика',
            'user_browsers' => 'Интернет-браузеры',
            'user_os' => 'Операционные системы',
            'user_devices' => 'Устройства',
            'impressions_limit' => 'Кол-во показов на 1 пользователя',
            'positions' => 'Должности',
            'ad_platform' => 'Рекламные площадки, на которых будет показываться объявление',
            'age_restriction' => 'Отображать отметку возрастного ограничения',
            'created_at' => 'Дата и время создания',
            'targetGroups' => 'Аудитории ретаргетинга',
            'targetGroupsNot' => 'Исключить аудитории ретаргетинга',
            'apps' => 'Приложения',
            'appsNot' => 'Исключить приложения',
            'eventsRetargetingGroupsGroup' => 'Аудитория ретаргетинга',
            'eventsRetargetingGroupsEvent' => 'События',
            'travellers' => 'Путешественники',
            'eventsRetargetingGroups' => 'События ретаргетинга',
            'schools' => 'Учебные заведения'
        ];
    }

    public static function typeSteps()
    {
        return [
            self::FORMAT_COMMUNITY_POST => [
                self::SCENARIO_LINK,
                self::SCENARIO_PRICES,
                self::SCENARIO_TARGETING,
                self::SCENARIO_NAME,
            ],
            self::FORMAT_IMAGE_TEXT => [
                self::SCENARIO_TITLE,
                self::SCENARIO_PHOTO,
                self::SCENARIO_PRICE_TRANSFER,
                self::SCENARIO_IMAGE_TEXT_TARGETING,
                self::SCENARIO_IMAGE_TEXT_NAME,
            ],
            self::FORMAT_BIG_IMAGE => [
                self::SCENARIO_BIG_IMAGE_TITLE,
                self::SCENARIO_BIG_IMAGE_PHOTO,
                self::SCENARIO_BIG_IMAGE_PRICE_TRANSFER,
                self::SCENARIO_BIG_IMAGE_TARGETING,
                self::SCENARIO_BIG_IMAGE_NAME,
            ],
            self::FORMAT_COMMUNITY => [
                self::SCENARIO_COMMUNITY_LINK,
                self::SCENARIO_COMMUNITY_PHOTO,
                self::SCENARIO_COMMUNITY_PRICE_TRANSFER,
                self::SCENARIO_COMMUNITY_TARGETING,
                self::SCENARIO_COMMUNITY_NAME,
            ],
        ];
    }

    /**
     * @return array
     */
    public function adPlatformLabels()
    {
        if($this->format == self::FORMAT_IMAGE_TEXT && $this->price_type == self::PRICE_TYPE_TRANSFER){
            return [
                '0' => 'ВКонтакте и сайты-партнёры',
                '1' => 'Только ВКонтакте'
            ];
        } else if(in_array($this->format, [self::FORMAT_COMMUNITY_POST, self::FORMAT_ADAPTIVE])){
            return [
                'all' => 'Все площадки',
                'desktop' => 'Полная версия сайта',
                'mobile' => 'Мобильный сайт и приложения'
            ];
        }

        return [];
    }

    /**
     * @return array
     */
    public function userBrowsersLabels()
    {
        return [
            '200' => 'Другие браузеры',
            '202' => 'Opera Mini',
            '207' => 'Firefox',
            '208' => 'Chrome',
            '209' => 'Safari',
            '210' => 'Internet Explorer',
            '211' => 'Opera',
            '216' => 'Yandex.Browser'
        ];
    }

    public function userOsLabels()
    {
        return [
            '220' => 'Windows XP/Vista',
            '221' => 'Windows 7',
            '222' => 'Windows 8',
            '226' => 'Windows 10',
            '223' => 'macOS',
            '224' => 'Linux'
        ];
    }

    public function userDevicesLabels()
    {
        return [
            '1001' => 'Все сматрфоны',
            '201' => 'Все мобильные телефоны',
            '1002' => 'Все планшеты',
            '237' => 'Android (Бюджетные)',
            '238' => 'Android (Средняя цена)',
            '239' => 'Android (Высокая цена)',
            '240' => 'iPhone XS/XR',
            '235' => 'iPhone X',
            '234' => 'iPhone 8',
            '233' => 'iPhone 7',
            '232' => 'iPhone 6/6S',
            '231' => 'iPhone 5/5S',
            '230' => 'iPhone 4/4S',
            '236' => 'iPod',
            '203' => 'iPhone/iPod Touch',
            '205' => 'iPad',
            '204' => 'Android-смартфон',
            '206' => 'Android-планшет',
            '212' => 'Windows Phone',
            '213' => 'BlackBerry'
        ];
    }

    public function eventsRetargetingGroupsLabels()
    {
        return [
            '1' => 'Просмотр промопоста',
            '2' => 'Переход по ссылке или переход в сообщество',
            '3' => 'Переход в сообщество',
            '4' => 'Подписка на сообщество',
            '5' => 'Отписка от новостей сообщества',
            '6' => 'Скрытие или жалоба',
            '10' => 'Запуск видео',
            '11' => 'Досмотр видео до 3с',
            '12' => 'Досмотр видео до 25%',
            '13' => 'Досмотр видео до 50%',
            '14' => 'Досмотр видео до 75%',
            '15' => 'Досмотр видео до 100%',
            '20' => 'Лайк продвигаемой записи',
            '21' => 'Репост продвигаемой записи',
        ];
    }

    public function impressionsLimitList()
    {
        $arr = [1, 2, 3, 5, 10, 15, 20];

        return array_combine($arr, $arr);
    }

    public static function getScenarioIndex($scenario)
    {
        $steps = self::typeSteps();

        foreach ($steps as $step) {
            for ($i = 0; $i < count($step); $i++) {
                $scnr = $step[$i];

                if($scenario == $scnr){
                    return $i;
                }
            }
        }

        return null;
    }

    public function nextStep()
    {
        if($this->format == null){
            $this->scenario = self::SCENARIO_FORMAT;
            return false;
        } else {
            // $this->step = $this->step + 1;
        }

        // if($this->scenario == self::SCENARIO_FORMAT){
        // $this->step = $this->step + 1;
        // }

        // $this->save('photo');


        $file = UploadedFile::getInstance($this, 'photo');
        if($file != null){
            $this->photo = $file;
        }

        // var_dump($file);

        // var_dump(getimagesize($file->tempName));

        // exit;

        if($this->validate() == false){
            // echo $this->price_type.'<br>';
            // echo $this->scenario;
            // exit;
            return false;
        } else if($this->isCurrentLastStep()) {
            // echo 'ok';
            // exit;

            // VarDumper::dump($this, 10, true);

            return $this->save(false);
        } else {
            $this->saveFile('photo');
            // exit;
            $this->step = $this->step + 1;
        }
        // if($this->isCurrentLastStep()){
        // return $this->save(false);
        // } else {


        // exit;

        $this->scenario = self::typeSteps()[$this->format][$this->step];
        // $this->step = $this->step+1;
        // $this->scenario = self::typeSteps()[$this->format][$this->step];
        // return false;
        // }

        return false;
    }

    /**
     * Сохранить файл
     * @param string $attribute
     */
    public function saveFile($attribute)
    {
        // $file = UploadedFile::getInstance($this, $attribute);
        $file = $this->$attribute;
        if($file == null || is_string($file)){
            return;
        }
        if(is_dir('uploads') == false){
            mkdir('uploads');
        }
        if(is_dir('uploads/photo') == false){
            mkdir('uploads/photo');
        }
        $fileName = Yii::$app->security->generateRandomString();
        $path = "uploads/photo/{$fileName}.{$file->extension}";

        $file->saveAs($path);

        $this->$attribute = $path;
    }

    public function isCurrentLastStep()
    {
        return $this->format != null ? ($this->step == (count(self::typeSteps()[$this->format]))-1) : false;
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_STOPPED => 'Остановлена',
            self::STATUS_ACTIVE => 'Активна',
        ];
    }

    /**
     * @param string $type
     * @return array
     */
    public static function priceTypeLabels($type = null)
    {
        if($type == AdvertisingCompany::TYPE_PROMOTED_POSTS){
            return [
                self::PRICE_TYPE_VIEW => 'За просмотры',
            ];
        }

        return [
            self::PRICE_TYPE_VIEW => 'За просмотры',
            self::PRICE_TYPE_TRANSFER => 'За переходы',
        ];
    }

    /**
     * @param string $type
     * @return array
     */
    public static function formatLabels($type)
    {
        if($type == AdvertisingCompany::TYPE_PROMOTED_POSTS) {
            return [
                self::FORMAT_COMMUNITY_POST => 'Запись в сообществе',
            ];
        } else if($type == AdvertisingCompany::TYPE_ADAPTIVE_ADS) {
            return [
                self::FORMAT_ADAPTIVE=> 'Адаптивный формат'
            ];
        } else {
            return [
                self::FORMAT_IMAGE_TEXT => 'Изображение и текст',
                self::FORMAT_BIG_IMAGE => 'Большое изображение',
                // self::FORMAT_COMMUNITY => 'Продвижение сообществ или приложений, квадратное изображение',
                // self::FORMAT_SPEC_COMMUNITY => 'Специальный формат сообществ',
                // self::FORMAT_COMMUNITY_POST => 'Запись в сообществе',
                // self::FORMAT_ADAPTIVE=> 'Адаптивный формат',
            ];
        }
        // return [
        //     self::FORMAT_IMAGE_TEXT => 'Изображение и текст',
        //     self::FORMAT_BIG_IMAGE => 'Большое изображение',
        //     self::FORMAT_COMMUNITY => 'Продвижение сообществ или приложений, квадратное изображение',
        //     self::FORMAT_SPEC_COMMUNITY => 'Специальный формат сообществ',
        //     self::FORMAT_COMMUNITY_POST => 'Запись в сообществе',
        //     self::FORMAT_ADAPTIVE=> 'Адаптивный формат',
        // ];
    }

    /**
     * @return array
     */
    public static function goalLabels()
    {
        return [
            self::GOAL_TYPE_VIEWS => 'Показы',
            self::GOAL_TYPE_TRANSFER => 'Переходы',
            self::GOAL_TYPE_SEND => 'Отправка заявок'
        ];
    }

    /**
     * @return array
     */
    public static function birthdayLabels()
    {
        return [
            1 => 'Сегодня',
            2 => 'Завтра',
            4 => 'В течении недели',
        ];
    }

    public static function sexLabels()
    {
        return [
            0 => 'Любой',
            1 => 'Женский',
            2 => 'Мужской',
        ];
    }

    public static function familyStatusLabels()
    {
        return [
            1 => 'Не женат/Не замужем',
            2 => 'Есть подруга/Есть друг',
            3 => 'Полмолвлен(а)',
            4 => 'Женат/Замужем',
            5 => 'Все сложно',
            6 => 'В активном поиске',
            7 => 'Влюблен/влюблена',
            8 => 'В гражданском браке',
        ];
    }

    public static function ageRestrictionLabels()
    {
        return [
            0 => 'Не отображать отметку',
            1 => '0+',
            2 => '6+',
            3 => '12+',
            4 => '16+',
            5 => '18+'
        ];
    }

    public static function ageRestrictionYears()
    {
        return [
            0 => -1,
            1 => 0,
            2 => 6,
            3 => 12,
            4 => 16,
            5 => 18
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $accountId = Settings::findByKey('account_id')->value;
        if($this->isNewRecord){

            // if($this->photo != null){
            //     $file = UploadedFile::getInstance($this, 'photo');
            //     if(is_dir('uploads') == false){
            //         mkdir('uploads');
            //     }
            //     if(is_dir('uploads/photo') == false){
            //         mkdir('uploads/photo');
            //     }
            //     $fileName = Yii::$app->security->generateRandomString();
            //     $path = "uploads/photo/{$fileName}.{$file->extension}";

            //     $file->saveAs($path);

            //     $this->photo = $path;
            // }


            // if($this->photo_icon != null){
            //     $file = UploadedFile::getInstance($this, 'photo_icon');
            //     if(is_dir('uploads') == false){
            //         mkdir('uploads');
            //     }
            //     if(is_dir('uploads/photo_icon') == false){
            //         mkdir('uploads/photo_icon');
            //     }
            //     $fileName = Yii::$app->security->generateRandomString();
            //     $path = "uploads/photo_icon/{$fileName}.{$file->extension}";

            //     $file->saveAs($path);

            //     $this->photo_icon = $path;
            // }

            $response = VkAPI::createAds($accountId, $this->company->company_id, $this);

            if(isset($response['response'][0])){
                if(ArrayHelper::getValue($response['response'][0], 'error_desc') != null){
                    if(ArrayHelper::getValue($response['response'][0], 'error_desc') == 'Some ads error occured: Invalid community post.'){
                        $this->addError('link_url', 'Не корректный пост');
                        return false;
                    }
                }
            }

            if(isset($response['response'][0])){
                if(ArrayHelper::getValue($response['response'][0], 'id') != null){
                    $this->advert_id = ArrayHelper::getValue($response['response'][0], 'id');
                }
            }
        } else {
            if($this->advert_id != null){
                $response = VkAPI::updateAds($accountId, $this, array_keys($this->getDirtyAttributes()));

                if(isset($response['response'][0])){
                    if(ArrayHelper::getValue($response['response'][0], 'error_desc') != null){
                        if(ArrayHelper::getValue($response['response'][0], 'error_desc') == 'Some ads error occured: Invalid community post.'){
                            $this->addError('link_url', 'Не корректный пост');
                            return false;
                        }
                    }
                }

                if(isset($response['response'][0])){
                    if(ArrayHelper::getValue($response['response'][0], 'id') != null){
                        $this->advert_id = ArrayHelper::getValue($response['response'][0], 'id');
                    }
                }
            }
        }

        $this->groups = '';
        $this->cities = '';
        $this->cities_not = '';
        $this->groups_not = '';
        $this->groups_active = '';
        $this->schools = '';
        $this->targetGroups = '';
        $this->targetGroupsNot = '';
        $this->user_browsers = '';
        $this->user_os = '';
        $this->user_devices = '';
        $this->positions = '';

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        var_dump($this->cities);

        if($this->cities != null){
            if(!$insert){
                TCity::deleteAll(['advert_id' => $this->id]);
            }
            TCity::createFromAdvert($this->id, $this->cities);
        }
    }

    public function setLastScenario()
    {
        $this->scenario = $this->getLastScenario();
    }

    public function getLastScenario()
    {
        $steps = self::typeSteps()[$this->format];
        return $steps[count($steps)-1];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(AdvertisingCompany::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
