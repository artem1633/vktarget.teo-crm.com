<?php

namespace app\models\dto;

use app\models\AdvertisingCompany;
use Yii;
use yii\helpers\VarDumper;

/**
 * Class AdvertisingCompanyToVk
 * @package app\models\dto
 */
class AdvertisingCompanyToVk
{
    /**
     * @var AdvertisingCompany
     */
    private $model;

    /**
     * AdvertisingCompanyToVk constructor.
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * @return array
     */
    public function transfer()
    {
       $data = [
           'name' => $this->model->name,
           'day_limit' => intval($this->model->day_limit),
           'all_limit' => intval($this->model->general_limit),
           'status' => intval($this->model->status),
           'type' => $this->model->type,
       ];

//       Yii::warning(json_encode([$data]));

       return $data;
    }
}