<?php

namespace app\models;

use Yii;
use app\components\VkAPI;

/**
 * This is the model class for table "t_group".
 *
 * @property int $id
 * @property int $advert_id Объявление
 * @property int $vk_id ID группы
 * @property string $title Наименование группы
 *
 * @property Advert $advert
 */
class TGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 't_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['advert_id', 'vk_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['advert_id'], 'exist', 'skipOnError' => true, 'targetClass' => Advert::className(), 'targetAttribute' => ['advert_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'advert_id' => 'Объявление',
            'vk_id' => 'ID группы',
            'title' => 'Наименование группы',
        ];
    }

    /**
     * @param integer $advertId
     * @param array $ids
     * @return boolean
     */
    public static function createFromAdvert($advertId, $ids)
    {
        if(count($ids) > 1000){
            return false;
        }

        $groups = VkAPI::getGroupsById($ids);

        if(isset($groups['response'])){
            foreach ($groups['response'] as $item){
                $group = new TGroup([
                    'advert_id' => $advertId,
                    'vk_id' => $item['id'],
                    'title' => $item['name']
                ]);
                $group->save(false);
            }
            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvert()
    {
        return $this->hasOne(Advert::className(), ['id' => 'advert_id']);
    }
}
