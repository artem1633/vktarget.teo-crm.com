<?php

namespace app\models\forms;

use app\components\VkAPI;
use yii\base\Model;

/**
 * Class VkPostForm
 * @package app\models\forms
 */
class VkPostForm extends Model
{
    /**
     * @var string
     */
    public $ownerLink;

    /**
     * @var string
     */
    public $message;

    /**
     * @var integer
     */
    public $signed;


    /**
     * @var integer
     */
    private $postId;

    /**
     * @var integer
     */
    private $ownerId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ownerLink', 'message', 'signed'], 'required'],
            [['signed'], 'safe'],
            ['message', 'string', 'max' => 220],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ownerLink' => 'Ссылка на группу',
            'message' => 'Текст',
            'signed' => 'Подпись'
        ];
    }

    /**
     * @return boolean
     */
    public function post()
    {
        if($this->validate()){
            $this->ownerId = $this->getOwnerId();

            if($this->ownerId == null){
                return false;
            }

            $ownerId = "-{$this->ownerId}";

            $response = VkAPI::postAdsStealth($ownerId, $this->message, $this->signed);

            if(isset($response['error'])){

                if($response['error']['error_code'] == 15){
                    $this->addError('ownerLink', 'У вас нет прав на эту группу');
                }

                return false;
            }

            if(isset($response['response']['post_id'])){
                $this->postId = $response['response']['post_id'];
                return true;
            }
        }

        return false;
    }

    public function getPostLink()
    {
        return "https://vk.com/wall-{$this->ownerId}_{$this->postId}";
    }

    private function getOwnerId()
    {
        $parts = explode('/', $this->ownerLink);

        if(count($parts) <= 1){
            $this->addError('ownerLink', 'Введите корректную ссылку');
            return null;
        }

        $groupName = $parts[count($parts) -1];

        $response = VkAPI::resolveScreenName($groupName);

        if(count($response['response']) == 0){
            $this->addError('ownerLink', 'Группы не существует или группа недоступна');
            return null;
        }

        if($response['response']['type'] != 'group'){
            $this->addError('ownerLink', 'Группа некорректна');
            return null;
        }

        return $response['response']['object_id'];
    }
}