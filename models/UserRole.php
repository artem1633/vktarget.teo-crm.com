<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_role".
 *
 * @property int $id
 * @property string $name Наименование
 * @property double $max_month_credits Предельный объем трат за 1 календарный мес
 * @property double $max_day_credits Предельный объем трат за 1 день
 * @property double $max_rate_click Предельная ставка за 1 клик
 * @property double $max_thousand_rate_view Предельная ставка за 1000 показов
 * @property double $max_credit_speed_in_value Предельная скорость трат в рублях
 * @property double $max_credit_speed_in_min Предельная скорость трат в минутах
 * @property string $created_at
 *
 * @property User[] $users
 */
class UserRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_role';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'max_month_credits', 'max_day_credits', 'max_rate_click', 'max_thousand_rate_view', 'max_credit_speed_in_value', 'max_credit_speed_in_min'], 'required'],
            [['max_month_credits', 'max_day_credits', 'max_rate_click', 'max_thousand_rate_view', 'max_credit_speed_in_value', 'max_credit_speed_in_min'], 'number'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'max_month_credits' => 'Предельный объем трат за 1 календарный мес',
            'max_day_credits' => 'Предельный объем трат за 1 день',
            'max_rate_click' => 'Предельная ставка за 1 клик',
            'max_thousand_rate_view' => 'Предельная ставка за 1000 показов',
            'max_credit_speed_in_value' => 'Предельная скорость трат в рублях',
            'max_credit_speed_in_min' => 'Предельная скорость трат в минутах',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['role_id' => 'id']);
    }
}
